/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.Afecciones;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author euge
 */
@Local
public interface AfeccionesFacadeLocal {

    void create(Afecciones afecciones);

    void edit(Afecciones afecciones);

    void remove(Afecciones afecciones);

    Afecciones find(Object id);

    List<Afecciones> findAll();

    List<Afecciones> findRange(int[] range);

    int count();
    
}
