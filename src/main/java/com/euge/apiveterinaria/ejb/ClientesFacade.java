/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.Clientes;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author euge
 */
@Stateless
public class ClientesFacade extends AbstractFacade<Clientes> implements ClientesFacadeLocal {

    @PersistenceContext(unitName = "com.euge_apiveterinaria_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ClientesFacade() {
        super(Clientes.class);
    }
    
    
    @Override
    public Clientes findByDni(String dni) {
        Clientes c = null;
        String consulta = "FROM Personas p WHERE p.dni = ?1";
        try {

            TypedQuery<Clientes> q = em.createQuery(consulta, Clientes.class);
            q.setParameter(1, dni);
            if (q.getSingleResult() != null) {
                c = q.getSingleResult();
            }
        } catch (Exception e) {
            System.out.println("FALLO : " + e.getLocalizedMessage());
        }
        return c;
    }
    
    
    @Override
    public List<Clientes> findByNameLike(String name) {
        Query query = em.createQuery("SELECT c FROM Clientes c WHERE lower(c.id.nombre) like :nombre");
        name = "%" + name.trim() + "%";
        return query.setParameter("nombre", name).getResultList();
    }

    @Override
    public List<Clientes> findByParams(int start, int size, String search) {
        String consulta = "FROM Clientes c WHERE lower(c.id.nombre) like :nombre";
        search = "%" + search.trim() + "%";
        Query q = em.createQuery(consulta);
        q.setFirstResult(start).setMaxResults(size);
        q.setParameter("nombre", search);
        List<Clientes> lstClientes = q.getResultList();
        return lstClientes;
    }
    
}
