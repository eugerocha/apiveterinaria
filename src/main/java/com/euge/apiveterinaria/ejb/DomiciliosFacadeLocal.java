/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.Domicilios;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author euge
 */
@Local
public interface DomiciliosFacadeLocal {

    void create(Domicilios domicilios);

    void edit(Domicilios domicilios);

    void remove(Domicilios domicilios);

    Domicilios find(Object id);

    List<Domicilios> findAll();

    List<Domicilios> findRange(int[] range);

    int count();
    
}
