/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.HorarioAtencion;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author euge
 */
@Local
public interface HorarioAtencionFacadeLocal {

    void create(HorarioAtencion horarioAtencion);

    void edit(HorarioAtencion horarioAtencion);

    void remove(HorarioAtencion horarioAtencion);

    HorarioAtencion find(Object id);

    List<HorarioAtencion> findAll();

    List<HorarioAtencion> findRange(int[] range);

    int count();
    
}
