/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.HorariosVeterinarias;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author euge
 */
@Stateless
public class HorariosVeterinariasFacade extends AbstractFacade<HorariosVeterinarias> implements HorariosVeterinariasFacadeLocal {

    @PersistenceContext(unitName = "com.euge_apiveterinaria_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HorariosVeterinariasFacade() {
        super(HorariosVeterinarias.class);
    }

    @Override
    public List<HorariosVeterinarias> findByIdVet(int id) {
       String consulta = "FROM HorariosVeterinarias h WHERE h.id_vet.id = ?1";
        Query q = em.createQuery(consulta);
        q.setParameter(1, id);
        List<HorariosVeterinarias> lstHorarios = q.getResultList();
        return lstHorarios; 
    }
    
}
