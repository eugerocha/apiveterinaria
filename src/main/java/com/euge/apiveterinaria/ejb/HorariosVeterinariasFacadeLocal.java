/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.HorariosVeterinarias;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author euge
 */
@Local
public interface HorariosVeterinariasFacadeLocal {

    void create(HorariosVeterinarias horariosVeterinarias);

    void edit(HorariosVeterinarias horariosVeterinarias);

    void remove(HorariosVeterinarias horariosVeterinarias);

    HorariosVeterinarias find(Object id);

    List<HorariosVeterinarias> findAll();

    List<HorariosVeterinarias> findRange(int[] range);

    int count();
    
    List<HorariosVeterinarias> findByIdVet(int id);
    
}
