/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.Mascotas;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author euge
 */
@Stateless
public class MascotasFacade extends AbstractFacade<Mascotas> implements MascotasFacadeLocal {

    @PersistenceContext(unitName = "com.euge_apiveterinaria_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MascotasFacade() {
        super(Mascotas.class);
    }

    @Override
    public List<Mascotas> findByIdPropietario(int id) {
        String consulta = "FROM Mascotas m WHERE m.idCliente.id.id = ?1";
        Query q = em.createQuery(consulta);
        q.setParameter(1, id);
        List<Mascotas> lstMascotas = q.getResultList();
        return lstMascotas;
    }

    @Override
    public List<Mascotas> findByNameLike(String name) {
        Query query = em.createQuery("SELECT m FROM Mascotas m WHERE lower(m.nombre) like :nombre");
        name = "%" + name.trim() + "%";
        return query.setParameter("nombre", name).getResultList();
    }

    @Override
    public List<Mascotas> findByParams(int start, int size, String search) {
        String consulta = "FROM Mascotas m WHERE lower(m.nombre) like :nombre";
        search = "%" + search.trim() + "%";
        Query q = em.createQuery(consulta);
        q.setFirstResult(start).setMaxResults(size);
        q.setParameter("nombre", search);
        List<Mascotas> lstMascotas = q.getResultList();
        return lstMascotas;
    }

    @Override
    public Mascotas findById(int id) {
        String consulta;
        Mascotas mascota = null;
        try {
            consulta = "FROM Mascotas m WHERE m.id=?1";
            TypedQuery<Mascotas> query = em.createQuery(consulta, Mascotas.class);
            query.setParameter(1, id);
            if (!query.getResultList().isEmpty()) {
                mascota = query.getResultList().get(0);
            }
        } catch (Exception e) {
            throw e;
        }
        return mascota;
    }
}
