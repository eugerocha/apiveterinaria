/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.Mascotas;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author euge
 */
@Local
public interface MascotasFacadeLocal {

    void create(Mascotas mascotas);

    void edit(Mascotas mascotas);

    void remove(Mascotas mascotas);

    Mascotas find(Object id);

    Mascotas findById(int id);

    List<Mascotas> findAll();

    List<Mascotas> findRange(int[] range);

    int count();

    List<Mascotas> findByIdPropietario(int id);

    List<Mascotas> findByNameLike(String name);

    List<Mascotas> findByParams(int start, int size, String search);

}
