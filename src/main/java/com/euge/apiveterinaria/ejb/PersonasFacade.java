/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.Personas;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author euge
 */
@Stateless
public class PersonasFacade extends AbstractFacade<Personas> implements PersonasFacadeLocal {

    @PersistenceContext(unitName = "com.euge_apiveterinaria_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PersonasFacade() {
        super(Personas.class);
    }
    
    
    @Override
    public Personas findByDni(String dni) {
        Personas p = null;
        String consulta = "FROM Personas p WHERE p.dni = ?1";
        try {

            TypedQuery<Personas> q = em.createQuery(consulta, Personas.class);
            q.setParameter(1, dni);
            if (q.getSingleResult() != null) {
                p = q.getSingleResult();
            }
        } catch (Exception e) {
            System.out.println("FALLO : " + e.getLocalizedMessage());
        }
        return p;
    }
}
