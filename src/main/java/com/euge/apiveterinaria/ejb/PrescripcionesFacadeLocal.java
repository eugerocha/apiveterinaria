/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.Prescripciones;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author euge
 */
@Local
public interface PrescripcionesFacadeLocal {

    void create(Prescripciones prescripciones);

    void edit(Prescripciones prescripciones);

    void remove(Prescripciones prescripciones);

    Prescripciones find(Object id);

    List<Prescripciones> findAll();

    List<Prescripciones> findRange(int[] range);

    int count();
    
}
