/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.Profesionales;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author euge
 */
@Stateless
public class ProfesionalesFacade extends AbstractFacade<Profesionales> implements ProfesionalesFacadeLocal {

    @PersistenceContext(unitName = "com.euge_apiveterinaria_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProfesionalesFacade() {
        super(Profesionales.class);
    }

    @Override
    public Profesionales findByDni(String dni) {
        Profesionales p = null;
        String consulta = "FROM Personas p WHERE p.dni = ?1";
        try {

            TypedQuery<Profesionales> q = em.createQuery(consulta, Profesionales.class);
            q.setParameter(1, dni);
            if (q.getSingleResult() != null) {
                p = q.getSingleResult();
            }
        } catch (Exception e) {
            System.out.println("FALLO : " + e.getLocalizedMessage());
        }
        return p;
    }
    
    @Override
    public List<Profesionales> findByNameLike(String name) {
        Query query = em.createQuery("SELECT p FROM Profesionales p WHERE lower(p.idProfesional.nombre) like :nombre");
        name = "%" + name.trim() + "%";
        return query.setParameter("nombre", name).getResultList();
    }
    
    @Override
    public List<Profesionales> findByParams(int start, int size, String search) {
        String consulta = "FROM Profesionales p WHERE lower(p.idProfesional.nombre) like :nombre";
        search = "%" + search.trim() + "%";
        Query q = em.createQuery(consulta);
        q.setFirstResult(start).setMaxResults(size);
        q.setParameter("nombre", search);
        List<Profesionales> lstProfesionales = q.getResultList();
        return lstProfesionales;
    }
    
}
