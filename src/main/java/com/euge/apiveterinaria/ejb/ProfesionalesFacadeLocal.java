/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.Profesionales;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author euge
 */
@Local
public interface ProfesionalesFacadeLocal {

    void create(Profesionales profesionales);

    void edit(Profesionales profesionales);

    void remove(Profesionales profesionales);

    Profesionales find(Object id);

    List<Profesionales> findAll();

    List<Profesionales> findRange(int[] range);

    int count();
    
    public Profesionales findByDni(String n);
    
    List<Profesionales> findByNameLike(String name);
    
    List<Profesionales> findByParams(int start, int size, String search);
    
}
