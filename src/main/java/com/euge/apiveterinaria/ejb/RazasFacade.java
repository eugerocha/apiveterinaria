/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.Razas;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author euge
 */
@Stateless
public class RazasFacade extends AbstractFacade<Razas> implements RazasFacadeLocal {

    @PersistenceContext(unitName = "com.euge_apiveterinaria_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RazasFacade() {
        super(Razas.class);
    }
    
    @Override
    public List<Razas> findByNameLike(String name) {
        Query query = em.createQuery("SELECT r FROM Razas r WHERE lower(r.nombreRaza) like :nombre");
        name = "%" + name.trim() + "%";
        return query.setParameter("nombre", name).getResultList();
    }

    @Override
    public List<Razas> findByTipoMascota(int id) {
       String consulta = "FROM Razas r WHERE r.tipo.id = ?1";
        Query q = em.createQuery(consulta);
        q.setParameter(1, id);
        List<Razas> lstRazas = q.getResultList();
        return lstRazas; 
    }
    
}
