/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.Razas;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author euge
 */
@Local
public interface RazasFacadeLocal {

    void create(Razas razas);

    void edit(Razas razas);

    void remove(Razas razas);

    Razas find(Object id);

    List<Razas> findAll();

    List<Razas> findRange(int[] range);

    int count();
    
    List<Razas> findByNameLike(String name);
    
    List<Razas> findByTipoMascota(int idTipoMascota);
    
}
