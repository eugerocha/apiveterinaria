/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.TipoMascota;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author euge
 */
@Stateless
public class TipoMascotaFacade extends AbstractFacade<TipoMascota> implements TipoMascotaFacadeLocal {

    @PersistenceContext(unitName = "com.euge_apiveterinaria_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipoMascotaFacade() {
        super(TipoMascota.class);
    }
    
    @Override
    public List<TipoMascota> findByNameLike(String name) {
        Query query = em.createQuery("SELECT t FROM TipoMascota t WHERE lower(t.nombreTipo) like :nombre");
        name = "%" + name.trim() + "%";
        return query.setParameter("nombre", name).getResultList();
    }

    
}
