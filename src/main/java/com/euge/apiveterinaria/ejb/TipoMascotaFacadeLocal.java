/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.TipoMascota;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author euge
 */
@Local
public interface TipoMascotaFacadeLocal {

    void create(TipoMascota tipoMascota);

    void edit(TipoMascota tipoMascota);

    void remove(TipoMascota tipoMascota);

    TipoMascota find(Object id);

    List<TipoMascota> findAll();

    List<TipoMascota> findRange(int[] range);

    int count();
    
    List<TipoMascota> findByNameLike(String name);
    
}
