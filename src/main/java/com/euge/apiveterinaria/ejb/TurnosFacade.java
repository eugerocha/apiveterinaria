/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.Mascotas;
import com.euge.apiveterinaria.entity.Turnos;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author euge
 */
@Stateless
public class TurnosFacade extends AbstractFacade<Turnos> implements TurnosFacadeLocal {

    @PersistenceContext(unitName = "com.euge_apiveterinaria_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TurnosFacade() {
        super(Turnos.class);
    }

    @Override
    public List<Turnos> findByParams(int start, int size, String search) {
           String consulta = "FROM Turnos m WHERE lower(m.idMascota.nombre) like :nombre";
        search = "%" + search.trim() + "%";
        Query q = em.createQuery(consulta);
        q.setFirstResult(start).setMaxResults(size);
        q.setParameter("nombre", search);
        List<Turnos> lstTurnos = q.getResultList();
        return lstTurnos;
    }
    
}
