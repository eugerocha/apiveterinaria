/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.Turnos;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author euge
 */
@Local
public interface TurnosFacadeLocal {

    void create(Turnos turnos);

    void edit(Turnos turnos);

    void remove(Turnos turnos);

    Turnos find(Object id);

    List<Turnos> findAll();

    List<Turnos> findRange(int[] range);

    int count();
    
    List<Turnos> findByParams(int start, int size, String search);
}
