/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.Usuarios;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author euge
 */
@Stateless
public class UsuariosFacade extends AbstractFacade<Usuarios> implements UsuariosFacadeLocal {

    @PersistenceContext(unitName = "com.euge_apiveterinaria_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuariosFacade() {
        super(Usuarios.class);
    }
    
    @Override
    public Usuarios findyByUserName(String username) {
        Usuarios usuario = null;
        String consulta = "FROM Usuarios u WHERE u.username = ?1";
        try {
            Query q = em.createQuery(consulta);
            q.setParameter(1, username);
            List<Usuarios> lstUsuarios = q.getResultList();
            if (!lstUsuarios.isEmpty()) {
                usuario = lstUsuarios.get(0);
            }
        } catch (Exception e) {
            throw e;
        }
        return usuario;
    }
    
    @Override
    public List<Usuarios> getPaginated(int start, int size) {
        String consulta = "FROM Usuarios u ";
        Query q = em.createQuery(consulta);
        q.setFirstResult(start).setMaxResults(size);
        List<Usuarios> lstUsuarios = q.getResultList();
        return lstUsuarios;
    }
    
}
