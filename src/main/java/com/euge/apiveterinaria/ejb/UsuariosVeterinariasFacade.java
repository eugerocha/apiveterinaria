/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.UsuariosVeterinarias;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author euge
 */
@Stateless
public class UsuariosVeterinariasFacade extends AbstractFacade<UsuariosVeterinarias> implements UsuariosVeterinariasFacadeLocal {

    @PersistenceContext(unitName = "com.euge_apiveterinaria_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuariosVeterinariasFacade() {
        super(UsuariosVeterinarias.class);
    }
    
}
