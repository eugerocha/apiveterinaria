/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.UsuariosVeterinarias;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author euge
 */
@Local
public interface UsuariosVeterinariasFacadeLocal {

    void create(UsuariosVeterinarias usuariosVeterinarias);

    void edit(UsuariosVeterinarias usuariosVeterinarias);

    void remove(UsuariosVeterinarias usuariosVeterinarias);

    UsuariosVeterinarias find(Object id);

    List<UsuariosVeterinarias> findAll();

    List<UsuariosVeterinarias> findRange(int[] range);

    int count();
    
}
