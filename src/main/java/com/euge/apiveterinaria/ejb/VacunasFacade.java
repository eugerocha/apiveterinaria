/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.Vacunas;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author euge
 */
@Stateless
public class VacunasFacade extends AbstractFacade<Vacunas> implements VacunasFacadeLocal {

    @PersistenceContext(unitName = "com.euge_apiveterinaria_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VacunasFacade() {
        super(Vacunas.class);
    }
    
    
     @Override
    public List<Vacunas> findByParams(int start, int size, String search) {
        String consulta = "FROM Vacunas v WHERE lower(v.nombre) like :nombre";
        search = "%" + search.trim() + "%";
        Query q = em.createQuery(consulta);
        q.setFirstResult(start).setMaxResults(size);
        q.setParameter("nombre", search);
        List<Vacunas> lstVacunas = q.getResultList();
        return lstVacunas;
    }
    
     @Override
    public List<Vacunas> findByTipoMascota(int id) {
       String consulta = "FROM Vacunas v WHERE v.tipoMascota.id = ?1";
        Query q = em.createQuery(consulta);
        q.setParameter(1, id);
        List<Vacunas> lstVacunas = q.getResultList();
        return lstVacunas; 
    }
}
