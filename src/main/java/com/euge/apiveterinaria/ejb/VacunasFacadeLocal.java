/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.Vacunas;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author euge
 */
@Local
public interface VacunasFacadeLocal {

    void create(Vacunas vacunas);

    void edit(Vacunas vacunas);

    void remove(Vacunas vacunas);

    Vacunas find(Object id);

    List<Vacunas> findAll();

    List<Vacunas> findRange(int[] range);

    int count();
    
    List<Vacunas> findByParams(int start, int size, String search);
    
    List<Vacunas> findByTipoMascota(int idTipoMascota);
    
}
