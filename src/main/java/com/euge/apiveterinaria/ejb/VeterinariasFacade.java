/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.Clientes;
import com.euge.apiveterinaria.entity.Veterinarias;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;


/**
 *
 * @author euge
 */
@Stateless
public class VeterinariasFacade extends AbstractFacade<Veterinarias> implements VeterinariasFacadeLocal {

    @PersistenceContext(unitName = "com.euge_apiveterinaria_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VeterinariasFacade() {
        super(Veterinarias.class);
    }
    
    
    @Override
    public Veterinarias findById(int id) {
        Veterinarias v = null;
        String consulta = "FROM Veterinarias v WHERE v.id = ?1";
        try {

            TypedQuery<Veterinarias> q = em.createQuery(consulta, Veterinarias.class);
            q.setParameter(1, id);
            if (q.getSingleResult() != null) {
                v = q.getSingleResult();
            }
        } catch (Exception e) {
            System.out.println("FALLO : " + e.getLocalizedMessage());
        }
        return v;
    }
    
}
