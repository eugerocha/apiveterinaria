/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.ejb;

import com.euge.apiveterinaria.entity.Veterinarias;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author euge
 */
@Local
public interface VeterinariasFacadeLocal {

    void create(Veterinarias veterinarias);

    void edit(Veterinarias veterinarias);

    void remove(Veterinarias veterinarias);

    Veterinarias find(Object id);

    List<Veterinarias> findAll();

    List<Veterinarias> findRange(int[] range);

    int count();
    
    public Veterinarias findById(int id);
    
}
