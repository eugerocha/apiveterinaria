/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author euge
 */
@Entity
@Table(name = "CalendarioVacunas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CalendarioVacunas.findAll", query = "SELECT c FROM CalendarioVacunas c"),
    @NamedQuery(name = "CalendarioVacunas.findById", query = "SELECT c FROM CalendarioVacunas c WHERE c.id = :id"),
    @NamedQuery(name = "CalendarioVacunas.findByFechaAplicacion", query = "SELECT c FROM CalendarioVacunas c WHERE c.fechaAplicacion = :fechaAplicacion"),
    @NamedQuery(name = "CalendarioVacunas.findByDosis", query = "SELECT c FROM CalendarioVacunas c WHERE c.dosis = :dosis"),
    @NamedQuery(name = "CalendarioVacunas.findByObservaciones", query = "SELECT c FROM CalendarioVacunas c WHERE c.observaciones = :observaciones"),
    @NamedQuery(name = "CalendarioVacunas.findByFechaCarga", query = "SELECT c FROM CalendarioVacunas c WHERE c.fechaCarga = :fechaCarga")})
public class CalendarioVacunas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "fecha_aplicacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAplicacion;
    @Column(name = "dosis")
    private Integer dosis;
    @Size(max = 300)
    @Column(name = "observaciones")
    private String observaciones;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_carga")
    @Temporal(TemporalType.DATE)
    private Date fechaCarga;
    @JoinColumn(name = "id_mascota", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Mascotas idMascota;
    @JoinColumn(name = "id_profesional", referencedColumnName = "id_profesional")
    @ManyToOne(optional = false)
    private Profesionales idProfesional;
    @JoinColumn(name = "id_vacuna", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Vacunas idVacuna;

    public CalendarioVacunas() {
    }

    public CalendarioVacunas(Integer id) {
        this.id = id;
    }

    public CalendarioVacunas(Integer id, Date fechaCarga) {
        this.id = id;
        this.fechaCarga = fechaCarga;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFechaAplicacion() {
        return fechaAplicacion;
    }

    public void setFechaAplicacion(Date fechaAplicacion) {
        this.fechaAplicacion = fechaAplicacion;
    }

    public Integer getDosis() {
        return dosis;
    }

    public void setDosis(Integer dosis) {
        this.dosis = dosis;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Date getFechaCarga() {
        return fechaCarga;
    }

    public void setFechaCarga(Date fechaCarga) {
        this.fechaCarga = fechaCarga;
    }

    public Mascotas getIdMascota() {
        return idMascota;
    }

    public void setIdMascota(Mascotas idMascota) {
        this.idMascota = idMascota;
    }

    public Profesionales getIdProfesional() {
        return idProfesional;
    }

    public void setIdProfesional(Profesionales idProfesional) {
        this.idProfesional = idProfesional;
    }

    public Vacunas getIdVacuna() {
        return idVacuna;
    }

    public void setIdVacuna(Vacunas idVacuna) {
        this.idVacuna = idVacuna;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CalendarioVacunas)) {
            return false;
        }
        CalendarioVacunas other = (CalendarioVacunas) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euge.apiveterinaria.entity.CalendarioVacunas[ id=" + id + " ]";
    }
    
}
