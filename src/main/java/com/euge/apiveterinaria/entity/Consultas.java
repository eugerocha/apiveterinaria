/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author euge
 */
@Entity
@Table(name = "Consultas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Consultas.findAll", query = "SELECT c FROM Consultas c"),
    @NamedQuery(name = "Consultas.findById", query = "SELECT c FROM Consultas c WHERE c.id = :id"),
    @NamedQuery(name = "Consultas.findByFecha", query = "SELECT c FROM Consultas c WHERE c.fecha = :fecha"),
    @NamedQuery(name = "Consultas.findByHora", query = "SELECT c FROM Consultas c WHERE c.hora = :hora"),
    @NamedQuery(name = "Consultas.findByMotivo", query = "SELECT c FROM Consultas c WHERE c.motivo = :motivo"),
    @NamedQuery(name = "Consultas.findByEstado", query = "SELECT c FROM Consultas c WHERE c.estado = :estado")})
public class Consultas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Column(name = "hora")
    @Temporal(TemporalType.TIME)
    private Date hora;
    @Size(max = 300)
    @Column(name = "motivo")
    private String motivo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private int estado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idConsulta")
    private Collection<Prescripciones> prescripcionesCollection;
    @JoinColumn(name = "id_mascota", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Mascotas idMascota;
    @JoinColumn(name = "id_profesional", referencedColumnName = "id_profesional")
    @ManyToOne(optional = false)
    private Profesionales idProfesional;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idConsulta")
    private Collection<Diagnosticos> diagnosticosCollection;

    public Consultas() {
    }

    public Consultas(Integer id) {
        this.id = id;
    }

    public Consultas(Integer id, int estado) {
        this.id = id;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        this.hora = hora;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    @XmlTransient
    public Collection<Prescripciones> getPrescripcionesCollection() {
        return prescripcionesCollection;
    }

    public void setPrescripcionesCollection(Collection<Prescripciones> prescripcionesCollection) {
        this.prescripcionesCollection = prescripcionesCollection;
    }


    public Mascotas getIdMascota() {
        return idMascota;
    }

    public void setIdMascota(Mascotas idMascota) {
        this.idMascota = idMascota;
    }

    public Profesionales getIdProfesional() {
        return idProfesional;
    }

    public void setIdProfesional(Profesionales idProfesional) {
        this.idProfesional = idProfesional;
    }

    @XmlTransient
    public Collection<Diagnosticos> getDiagnosticosCollection() {
        return diagnosticosCollection;
    }

    public void setDiagnosticosCollection(Collection<Diagnosticos> diagnosticosCollection) {
        this.diagnosticosCollection = diagnosticosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Consultas)) {
            return false;
        }
        Consultas other = (Consultas) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euge.apiveterinaria.entity.Consultas[ id=" + id + " ]";
    }
    
}
