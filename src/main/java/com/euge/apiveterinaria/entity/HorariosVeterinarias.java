/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author euge
 */
@Entity
@Table(name = "HorariosVeterinarias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "HorariosVeterinarias.findAll", query = "SELECT h FROM HorariosVeterinarias h"),
    @NamedQuery(name = "HorariosVeterinarias.findById", query = "SELECT h FROM HorariosVeterinarias h WHERE h.id = :id"),
    @NamedQuery(name = "HorariosVeterinarias.findByDia", query = "SELECT h FROM HorariosVeterinarias h WHERE h.dia = :dia"),
    @NamedQuery(name = "HorariosVeterinarias.findByApertura", query = "SELECT h FROM HorariosVeterinarias h WHERE h.apertura = :apertura"),
    @NamedQuery(name = "HorariosVeterinarias.findByCierre", query = "SELECT h FROM HorariosVeterinarias h WHERE h.cierre = :cierre"),
    @NamedQuery(name = "HorariosVeterinarias.findByObservaciones", query = "SELECT h FROM HorariosVeterinarias h WHERE h.observaciones = :observaciones")})
public class HorariosVeterinarias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "dia")
    private Integer dia;
    @Column(name = "apertura")
    @Temporal(TemporalType.TIME)
    private Date apertura;
    @Column(name = "cierre")
    @Temporal(TemporalType.TIME)
    private Date cierre;
    @Size(max = 150)
    @Column(name = "observaciones")
    private String observaciones;
    @JoinColumn(name = "id_vet", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Veterinarias idVet;

    public HorariosVeterinarias() {
    }

    public HorariosVeterinarias(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDia() {
        return dia;
    }

    public void setDia(Integer dia) {
        this.dia = dia;
    }

    public Date getApertura() {
        return apertura;
    }

    public void setApertura(Date apertura) {
        this.apertura = apertura;
    }

    public Date getCierre() {
        return cierre;
    }

    public void setCierre(Date cierre) {
        this.cierre = cierre;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Veterinarias getIdVet() {
        return idVet;
    }

    public void setIdVet(Veterinarias idVet) {
        this.idVet = idVet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HorariosVeterinarias)) {
            return false;
        }
        HorariosVeterinarias other = (HorariosVeterinarias) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euge.apiveterinaria.entity.HorariosVeterinarias[ id=" + id + " ]";
    }
    
}
