/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author euge
 */
@Entity
@Table(name = "Mascotas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mascotas.findAll", query = "SELECT m FROM Mascotas m"),
    @NamedQuery(name = "Mascotas.findById", query = "SELECT m FROM Mascotas m WHERE m.id = :id"),
    @NamedQuery(name = "Mascotas.findByNombre", query = "SELECT m FROM Mascotas m WHERE m.nombre = :nombre"),
    @NamedQuery(name = "Mascotas.findBySexo", query = "SELECT m FROM Mascotas m WHERE m.sexo = :sexo"),
    @NamedQuery(name = "Mascotas.findByFechanacimiento", query = "SELECT m FROM Mascotas m WHERE m.fechanacimiento = :fechanacimiento"),
    @NamedQuery(name = "Mascotas.findByPeso", query = "SELECT m FROM Mascotas m WHERE m.peso = :peso"),
    @NamedQuery(name = "Mascotas.findByImagen", query = "SELECT m FROM Mascotas m WHERE m.imagen = :imagen")})
public class Mascotas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 30)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sexo")
    private int sexo;
    @Column(name = "fechanacimiento")
    @Temporal(TemporalType.DATE)
    private Date fechanacimiento;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "peso")
    private Float peso;
    @Size(max = 300)
    @Column(name = "ant_quirurgicos")
    private String antQuirurgicos;
    @Size(max = 300)
    @Column(name = "ant_patologicos")
    private String antPatologicos;
    @Size(max = 300)
    @Column(name = "ant_traumatologicos")
    private String antTraumatologicos;

    @Size(max = 150)
    @Column(name = "imagen")
    private String imagen;
    @JoinTable(name = "MascotasVisitas", joinColumns = {
        @JoinColumn(name = "id_mascota", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "id_visita", referencedColumnName = "id")})
    @ManyToMany
    private Collection<Visitas> visitasCollection;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Clientes idCliente;
    @JoinColumn(name = "id_raza", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Razas idRaza;
    @JoinColumn(name = "id_tipo", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoMascota idTipo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMascota")
    private Collection<Consultas> consultasCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMascota")
    private Collection<CalendarioVacunas> calendarioVacunasCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMascota")
    private Collection<Visitas> visitasCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMascota")
    private Collection<Turnos> turnosCollection;

    public Mascotas() {
    }

    public Mascotas(Integer id) {
        this.id = id;
    }

    public Mascotas(Integer id, int sexo) {
        this.id = id;
        this.sexo = sexo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getSexo() {
        return sexo;
    }

    public void setSexo(int sexo) {
        this.sexo = sexo;
    }

    public Date getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(Date fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public Float getPeso() {
        return peso;
    }

    public void setPeso(Float peso) {
        this.peso = peso;
    }

    public String getAntQuirurgicos() {
        return antQuirurgicos;
    }

    public void setAntQuirurgicos(String antQuirurgicos) {
        this.antQuirurgicos = antQuirurgicos;
    }

    public String getAntPatologicos() {
        return antPatologicos;
    }

    public void setAntPatologicos(String antPatologicos) {
        this.antPatologicos = antPatologicos;
    }

    public String getAntTraumatologicos() {
        return antTraumatologicos;
    }

    public void setAntTraumatologicos(String antTraumatologicos) {
        this.antTraumatologicos = antTraumatologicos;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    @XmlTransient
    public Collection<Visitas> getVisitasCollection() {
        return visitasCollection;
    }

    public void setVisitasCollection(Collection<Visitas> visitasCollection) {
        this.visitasCollection = visitasCollection;
    }

    public Clientes getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Clientes idCliente) {
        this.idCliente = idCliente;
    }

    public Razas getIdRaza() {
        return idRaza;
    }

    public void setIdRaza(Razas idRaza) {
        this.idRaza = idRaza;
    }

    public TipoMascota getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(TipoMascota idTipo) {
        this.idTipo = idTipo;
    }

    @XmlTransient
    public Collection<Consultas> getConsultasCollection() {
        return consultasCollection;
    }

    public void setConsultasCollection(Collection<Consultas> consultasCollection) {
        this.consultasCollection = consultasCollection;
    }

    @XmlTransient
    public Collection<CalendarioVacunas> getCalendarioVacunasCollection() {
        return calendarioVacunasCollection;
    }

    public void setCalendarioVacunasCollection(Collection<CalendarioVacunas> calendarioVacunasCollection) {
        this.calendarioVacunasCollection = calendarioVacunasCollection;
    }

    @XmlTransient
    public Collection<Visitas> getVisitasCollection1() {
        return visitasCollection1;
    }

    public void setVisitasCollection1(Collection<Visitas> visitasCollection1) {
        this.visitasCollection1 = visitasCollection1;
    }

    @XmlTransient
    public Collection<Turnos> getTurnosCollection() {
        return turnosCollection;
    }

    public void setTurnosCollection(Collection<Turnos> turnosCollection) {
        this.turnosCollection = turnosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mascotas)) {
            return false;
        }
        Mascotas other = (Mascotas) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euge.apiveterinaria.entity.Mascotas[ id=" + id + " ]";
    }

}
