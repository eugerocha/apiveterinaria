/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author euge
 */
@Entity
@Table(name = "Prescripciones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Prescripciones.findAll", query = "SELECT p FROM Prescripciones p"),
    @NamedQuery(name = "Prescripciones.findById", query = "SELECT p FROM Prescripciones p WHERE p.id = :id"),
    @NamedQuery(name = "Prescripciones.findByProducto", query = "SELECT p FROM Prescripciones p WHERE p.producto = :producto"),
    @NamedQuery(name = "Prescripciones.findByCantidad", query = "SELECT p FROM Prescripciones p WHERE p.cantidad = :cantidad"),
    @NamedQuery(name = "Prescripciones.findByIndicaciones", query = "SELECT p FROM Prescripciones p WHERE p.indicaciones = :indicaciones")})
public class Prescripciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 20)
    @Column(name = "producto")
    private String producto;
    @Size(max = 20)
    @Column(name = "cantidad")
    private String cantidad;
    @Size(max = 300)
    @Column(name = "indicaciones")
    private String indicaciones;
    @JoinColumn(name = "id_consulta", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Consultas idConsulta;

    public Prescripciones() {
    }

    public Prescripciones(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getIndicaciones() {
        return indicaciones;
    }

    public void setIndicaciones(String indicaciones) {
        this.indicaciones = indicaciones;
    }

    public Consultas getIdConsulta() {
        return idConsulta;
    }

    public void setIdConsulta(Consultas idConsulta) {
        this.idConsulta = idConsulta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Prescripciones)) {
            return false;
        }
        Prescripciones other = (Prescripciones) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euge.apiveterinaria.entity.Prescripciones[ id=" + id + " ]";
    }
    
}
