/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author euge
 */
@Entity
@Table(name = "Profesionales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Profesionales.findAll", query = "SELECT p FROM Profesionales p"),
    @NamedQuery(name = "Profesionales.findByIdProfesional", query = "SELECT p FROM Profesionales p WHERE p.idProfesional = :idProfesional"),
    @NamedQuery(name = "Profesionales.findByLegajo", query = "SELECT p FROM Profesionales p WHERE p.legajo = :legajo"),
    @NamedQuery(name = "Profesionales.findByEstado", query = "SELECT p FROM Profesionales p WHERE p.estado = :estado")})
public class Profesionales implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "id_profesional",referencedColumnName = "id", nullable = false)
    private Personas idProfesional;
    @Size(max = 15)
    @Column(name = "legajo")
    private String legajo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private int estado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProfesional")
    private Collection<HorarioAtencion> horarioAtencionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProfesional")
    private Collection<Consultas> consultasCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProfesional")
    private Collection<CalendarioVacunas> calendarioVacunasCollection;
    @JoinColumn(name = "id_profesional", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Personas personas;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProfesional")
    private Collection<Turnos> turnosCollection;

    public Profesionales() {
    }


    public Personas getIdProfesional() {
        return idProfesional;
    }

    public void setIdProfesional(Personas idProfesional) {
        this.idProfesional = idProfesional;
    }

    
    public String getLegajo() {
        return legajo;
    }

    public void setLegajo(String legajo) {
        this.legajo = legajo;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    @XmlTransient
    public Collection<HorarioAtencion> getHorarioAtencionCollection() {
        return horarioAtencionCollection;
    }

    public void setHorarioAtencionCollection(Collection<HorarioAtencion> horarioAtencionCollection) {
        this.horarioAtencionCollection = horarioAtencionCollection;
    }

    @XmlTransient
    public Collection<Consultas> getConsultasCollection() {
        return consultasCollection;
    }

    public void setConsultasCollection(Collection<Consultas> consultasCollection) {
        this.consultasCollection = consultasCollection;
    }

    @XmlTransient
    public Collection<CalendarioVacunas> getCalendarioVacunasCollection() {
        return calendarioVacunasCollection;
    }

    public void setCalendarioVacunasCollection(Collection<CalendarioVacunas> calendarioVacunasCollection) {
        this.calendarioVacunasCollection = calendarioVacunasCollection;
    }

    public Personas getPersonas() {
        return personas;
    }

    public void setPersonas(Personas personas) {
        this.personas = personas;
    }

    @XmlTransient
    public Collection<Turnos> getTurnosCollection() {
        return turnosCollection;
    }

    public void setTurnosCollection(Collection<Turnos> turnosCollection) {
        this.turnosCollection = turnosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProfesional != null ? idProfesional.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Profesionales)) {
            return false;
        }
        Profesionales other = (Profesionales) object;
        if ((this.idProfesional == null && other.idProfesional != null) || (this.idProfesional != null && !this.idProfesional.equals(other.idProfesional))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euge.apiveterinaria.entity.Profesionales[ idProfesional=" + idProfesional + " ]";
    }
    
}
