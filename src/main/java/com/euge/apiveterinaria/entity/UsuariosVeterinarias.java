/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.entity;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author euge
 */
@Entity
@Table(name = "UsuariosVeterinarias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuariosVeterinarias.findAll", query = "SELECT u FROM UsuariosVeterinarias u"),
    @NamedQuery(name = "UsuariosVeterinarias.findByIdVeterinaria", query = "SELECT u FROM UsuariosVeterinarias u WHERE u.usuariosVeterinariasPK.idVeterinaria = :idVeterinaria"),
    @NamedQuery(name = "UsuariosVeterinarias.findByIdUsuario", query = "SELECT u FROM UsuariosVeterinarias u WHERE u.usuariosVeterinariasPK.idUsuario = :idUsuario"),
    @NamedQuery(name = "UsuariosVeterinarias.findByIdRol", query = "SELECT u FROM UsuariosVeterinarias u WHERE u.usuariosVeterinariasPK.idRol = :idRol")})
public class UsuariosVeterinarias implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UsuariosVeterinariasPK usuariosVeterinariasPK;
    @JoinColumn(name = "id_rol", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Roles roles;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Usuarios usuarios;
    @JoinColumn(name = "id_veterinaria", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Veterinarias veterinarias;

    public UsuariosVeterinarias() {
    }

    public UsuariosVeterinarias(UsuariosVeterinariasPK usuariosVeterinariasPK) {
        this.usuariosVeterinariasPK = usuariosVeterinariasPK;
    }

    public UsuariosVeterinarias(int idVeterinaria, long idUsuario, int idRol) {
        this.usuariosVeterinariasPK = new UsuariosVeterinariasPK(idVeterinaria, idUsuario, idRol);
    }

    public UsuariosVeterinariasPK getUsuariosVeterinariasPK() {
        return usuariosVeterinariasPK;
    }

    public void setUsuariosVeterinariasPK(UsuariosVeterinariasPK usuariosVeterinariasPK) {
        this.usuariosVeterinariasPK = usuariosVeterinariasPK;
    }

    public Roles getRoles() {
        return roles;
    }

    public void setRoles(Roles roles) {
        this.roles = roles;
    }

    public Usuarios getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Usuarios usuarios) {
        this.usuarios = usuarios;
    }

    public Veterinarias getVeterinarias() {
        return veterinarias;
    }

    public void setVeterinarias(Veterinarias veterinarias) {
        this.veterinarias = veterinarias;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuariosVeterinariasPK != null ? usuariosVeterinariasPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuariosVeterinarias)) {
            return false;
        }
        UsuariosVeterinarias other = (UsuariosVeterinarias) object;
        if ((this.usuariosVeterinariasPK == null && other.usuariosVeterinariasPK != null) || (this.usuariosVeterinariasPK != null && !this.usuariosVeterinariasPK.equals(other.usuariosVeterinariasPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euge.apiveterinaria.entity.UsuariosVeterinarias[ usuariosVeterinariasPK=" + usuariosVeterinariasPK + " ]";
    }
    
}
