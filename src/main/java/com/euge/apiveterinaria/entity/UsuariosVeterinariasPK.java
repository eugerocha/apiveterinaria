/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author euge
 */
@Embeddable
public class UsuariosVeterinariasPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_veterinaria")
    private int idVeterinaria;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_usuario")
    private long idUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_rol")
    private int idRol;

    public UsuariosVeterinariasPK() {
    }

    public UsuariosVeterinariasPK(int idVeterinaria, long idUsuario, int idRol) {
        this.idVeterinaria = idVeterinaria;
        this.idUsuario = idUsuario;
        this.idRol = idRol;
    }

    public int getIdVeterinaria() {
        return idVeterinaria;
    }

    public void setIdVeterinaria(int idVeterinaria) {
        this.idVeterinaria = idVeterinaria;
    }

    public long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdRol() {
        return idRol;
    }

    public void setIdRol(int idRol) {
        this.idRol = idRol;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idVeterinaria;
        hash += (int) idUsuario;
        hash += (int) idRol;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuariosVeterinariasPK)) {
            return false;
        }
        UsuariosVeterinariasPK other = (UsuariosVeterinariasPK) object;
        if (this.idVeterinaria != other.idVeterinaria) {
            return false;
        }
        if (this.idUsuario != other.idUsuario) {
            return false;
        }
        if (this.idRol != other.idRol) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euge.apiveterinaria.entity.UsuariosVeterinariasPK[ idVeterinaria=" + idVeterinaria + ", idUsuario=" + idUsuario + ", idRol=" + idRol + " ]";
    }
    
}
