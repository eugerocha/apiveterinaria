/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author euge
 */
@Entity
@Table(name = "Vacunas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vacunas.findAll", query = "SELECT v FROM Vacunas v"),
    @NamedQuery(name = "Vacunas.findById", query = "SELECT v FROM Vacunas v WHERE v.id = :id"),
    @NamedQuery(name = "Vacunas.findByNombre", query = "SELECT v FROM Vacunas v WHERE v.nombre = :nombre"),
    @NamedQuery(name = "Vacunas.findByEdadAplicacion", query = "SELECT v FROM Vacunas v WHERE v.edadAplicacion = :edadAplicacion"),
    @NamedQuery(name = "Vacunas.findByDetalle", query = "SELECT v FROM Vacunas v WHERE v.detalle = :detalle")})
public class Vacunas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 30)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 30)
    @Column(name = "edad_aplicacion")
    private String edadAplicacion;
    @Size(max = 150)
    @Column(name = "detalle")
    private String detalle;
    @JoinColumn(name = "tipo_mascota", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TipoMascota tipoMascota;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idVacuna")
    private Collection<CalendarioVacunas> calendarioVacunasCollection;

    public Vacunas() {
    }

    public Vacunas(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEdadAplicacion() {
        return edadAplicacion;
    }

    public void setEdadAplicacion(String edadAplicacion) {
        this.edadAplicacion = edadAplicacion;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public TipoMascota getTipoMascota() {
        return tipoMascota;
    }

    public void setTipoMascota(TipoMascota tipoMascota) {
        this.tipoMascota = tipoMascota;
    }

    @XmlTransient
    public Collection<CalendarioVacunas> getCalendarioVacunasCollection() {
        return calendarioVacunasCollection;
    }

    public void setCalendarioVacunasCollection(Collection<CalendarioVacunas> calendarioVacunasCollection) {
        this.calendarioVacunasCollection = calendarioVacunasCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vacunas)) {
            return false;
        }
        Vacunas other = (Vacunas) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euge.apiveterinaria.entity.Vacunas[ id=" + id + " ]";
    }
    
}
