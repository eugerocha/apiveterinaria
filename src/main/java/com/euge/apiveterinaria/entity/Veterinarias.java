/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;
import java.util.Collection;
import javax.annotation.Nullable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.DefaultValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author euge
 */
@Entity
@Table(name = "Veterinarias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Veterinarias.findAll", query = "SELECT v FROM Veterinarias v"),
    @NamedQuery(name = "Veterinarias.findById", query = "SELECT v FROM Veterinarias v WHERE v.id = :id"),
    @NamedQuery(name = "Veterinarias.findByRazonSocial", query = "SELECT v FROM Veterinarias v WHERE v.razonSocial = :razonSocial"),
    @NamedQuery(name = "Veterinarias.findByDomicilio", query = "SELECT v FROM Veterinarias v WHERE v.domicilio = :domicilio"),
    @NamedQuery(name = "Veterinarias.findByCuit", query = "SELECT v FROM Veterinarias v WHERE v.cuit = :cuit"),
    @NamedQuery(name = "Veterinarias.findByTelefono", query = "SELECT v FROM Veterinarias v WHERE v.telefono = :telefono"),
    @NamedQuery(name = "Veterinarias.findByEmail", query = "SELECT v FROM Veterinarias v WHERE v.email = :email"),
    @NamedQuery(name = "Veterinarias.findByEncargado", query = "SELECT v FROM Veterinarias v WHERE v.encargado = :encargado"),
    @NamedQuery(name = "Veterinarias.findByEstado", query = "SELECT v FROM Veterinarias v WHERE v.estado = :estado"),
    @NamedQuery(name = "Veterinarias.findByImagen", query = "SELECT v FROM Veterinarias v WHERE v.imagen = :imagen")})
public class Veterinarias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "razonSocial")
    private String razonSocial;
    @Size(max = 15)
    @Column(name = "domicilio")
    private String domicilio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 14)
    @Column(name = "cuit")
    private String cuit;
    @Size(max = 15)
    @Column(name = "telefono")
    private String telefono;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 30)
    @Column(name = "email")
    private String email;
    @Size(max = 50)
    @Column(name = "encargado")
    private String encargado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private int estado;
    @Size(max = 150)
    @Column(name = "imagen")
    @Nullable
    private String imagen;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idVeterinaria")
    private Collection<Servicios> serviciosCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idVet")
    private Collection<HorariosVeterinarias> horariosVeterinariasCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "veterinarias")
    private Collection<UsuariosVeterinarias> usuariosVeterinariasCollection;

    public Veterinarias() {
    }

    public Veterinarias(Integer id) {
        this.id = id;
    }

    public Veterinarias(Integer id, String razonSocial, String cuit, int estado) {
        this.id = id;
        this.razonSocial = razonSocial;
        this.cuit = cuit;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getCuit() {
        return cuit;
    }

    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEncargado() {
        return encargado;
    }

    public void setEncargado(String encargado) {
        this.encargado = encargado;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    @XmlTransient
    public Collection<Servicios> getServiciosCollection() {
        return serviciosCollection;
    }

    public void setServiciosCollection(Collection<Servicios> serviciosCollection) {
        this.serviciosCollection = serviciosCollection;
    }

    @XmlTransient
    public Collection<HorariosVeterinarias> getHorariosVeterinariasCollection() {
        return horariosVeterinariasCollection;
    }

    public void setHorariosVeterinariasCollection(Collection<HorariosVeterinarias> horariosVeterinariasCollection) {
        this.horariosVeterinariasCollection = horariosVeterinariasCollection;
    }

    @XmlTransient
    public Collection<UsuariosVeterinarias> getUsuariosVeterinariasCollection() {
        return usuariosVeterinariasCollection;
    }

    public void setUsuariosVeterinariasCollection(Collection<UsuariosVeterinarias> usuariosVeterinariasCollection) {
        this.usuariosVeterinariasCollection = usuariosVeterinariasCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Veterinarias)) {
            return false;
        }
        Veterinarias other = (Veterinarias) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euge.apiveterinaria.entity.Veterinarias[ id=" + id + " ]";
    }
    
}
