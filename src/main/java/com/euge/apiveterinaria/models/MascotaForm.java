/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;

/**
 *
 * @author fcastillo
 */
@ApiModel(value = "MascotaFormModel")
public class MascotaForm {

    @ApiModelProperty(position = 1)
    private int id;
    @ApiModelProperty(position = 2, required = true)
    private String nombre;
    @ApiModelProperty(position = 3, required = true)
    private int sexo;
    @ApiModelProperty(position = 4, required = true)
    private Date fnacimiento;
    @ApiModelProperty(position = 5, required = true, example = "55.5")
    private Float peso;
    @ApiModelProperty(position = 6)
    private String antecQuirurgicos;
    @ApiModelProperty(position = 7)
    private String antecPatologicos;
    @ApiModelProperty(position = 8)
    private String antecTraumatologicos;
    @ApiModelProperty(position = 9)
    private Long idPropietario;
    @ApiModelProperty(position = 10, required = true, allowableValues = "1,2,3,4,5" ,dataType = "Integer")
    private int tipoMascota;
    @ApiModelProperty(position = 11, required = true)
    private int raza;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getSexo() {
        return sexo;
    }

    public void setSexo(int sexo) {
        this.sexo = sexo;
    }

    public Date getFnacimiento() {
        return fnacimiento;
    }

    public void setFnacimiento(Date fnacimiento) {
        this.fnacimiento = fnacimiento;
    }

    public Float getPeso() {
        return peso;
    }

    public void setPeso(Float peso) {
        this.peso = peso;
    }

    public String getAntecQuirurgicos() {
        return antecQuirurgicos;
    }

    public void setAntecQuirurgicos(String antecQuirurgicos) {
        this.antecQuirurgicos = antecQuirurgicos;
    }

    public String getAntecPatologicos() {
        return antecPatologicos;
    }

    public void setAntecPatologicos(String antecPatologicos) {
        this.antecPatologicos = antecPatologicos;
    }

    public String getAntecTraumatologicos() {
        return antecTraumatologicos;
    }

    public void setAntecTraumatologicos(String antecTraumatologicos) {
        this.antecTraumatologicos = antecTraumatologicos;
    }

    public Long getIdPropietario() {
        return idPropietario;
    }

    public void setIdPropietario(Long idPropietario) {
        this.idPropietario = idPropietario;
    }

    public int getTipoMascota() {
        return tipoMascota;
    }

    public void setTipoMascota(int tipoMascota) {
        this.tipoMascota = tipoMascota;
    }

    public int getRaza() {
        return raza;
    }

    public void setRaza(int raza) {
        this.raza = raza;
    }
    

}
