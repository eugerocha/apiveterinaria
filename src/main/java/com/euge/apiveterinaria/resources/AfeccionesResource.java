/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.resources;

import com.euge.apiveterinaria.ejb.AfeccionesFacadeLocal;
import com.euge.apiveterinaria.entity.Afecciones;
import io.swagger.annotations.Api;
import java.util.List;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author euge
 */

@Path("/Afecciones")
@Api(value = "Afecciones")
public class AfeccionesResource {
    
  @EJB
  AfeccionesFacadeLocal afeccionEJB;
  
  
  @Path("/GetLista")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll(@QueryParam("start") int start, @QueryParam("size") int size, @QueryParam("search") String search) {
        List<Afecciones> lstAfecciones = afeccionEJB.findAll();
        JsonObjectBuilder job = Json.createObjectBuilder();
        JsonArrayBuilder jab = Json.createArrayBuilder();
        int totalRegistros = afeccionEJB.count();
        lstAfecciones.stream().forEach(a -> {
            job.add("id", a.getId())
                    .add("afección", a.getNombre() )
                    .add("síntomas", a.getSintomas());
            jab.add(job);
        });
        JsonObjectBuilder respuesta = Json.createObjectBuilder().add("status", Json.createObjectBuilder()
                .add("statusCode", 200).add("totalRows", totalRegistros))
                .add("afecciones", jab);
        return Response.status(Response.Status.OK).entity(respuesta.build()).build();

    }
    
    
}
