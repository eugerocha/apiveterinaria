/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.resources;

import com.euge.apiveterinaria.ejb.CalendarioVacunasFacadeLocal;
import com.euge.apiveterinaria.ejb.MascotasFacadeLocal;
import com.euge.apiveterinaria.ejb.PersonasFacadeLocal;
import com.euge.apiveterinaria.ejb.ProfesionalesFacadeLocal;
import com.euge.apiveterinaria.ejb.VacunasFacadeLocal;
import com.euge.apiveterinaria.entity.CalendarioVacunas;
import com.euge.apiveterinaria.entity.Mascotas;
import com.euge.apiveterinaria.entity.Personas;
import com.euge.apiveterinaria.entity.Profesionales;
import com.euge.apiveterinaria.entity.Vacunas;
import com.euge.apiveterinaria.utils.Utilidades;
import io.swagger.annotations.Api;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author euge
 */

@Path("CalendarioVacunas")
@Api(value = "CalendarioVacunas")
public class CalendarioVacunasResource {
    
    @EJB
    CalendarioVacunasFacadeLocal calendarioEJB;
    
    @EJB
    MascotasFacadeLocal mascotaEJB;
    
    @EJB
    VacunasFacadeLocal vacunaEJB;
    
    @EJB
    ProfesionalesFacadeLocal profesionalEJB;
    
     @EJB
    PersonasFacadeLocal personaEJB;
    
    @Path("/GetLista")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll(@QueryParam("start") int start, @QueryParam("size") int size, @QueryParam("search") String search) {
        List<CalendarioVacunas> lstCalendario = calendarioEJB.findAll();
        JsonObjectBuilder job = Json.createObjectBuilder();
        JsonArrayBuilder jab = Json.createArrayBuilder();
        int totalRegistros = calendarioEJB.count();
        lstCalendario.stream().forEach(c -> {
            job.add("id", c.getId())
                    .add("nombre_mascota", c.getIdMascota().getNombre() )
                    .add("vacuna", c.getIdVacuna().getNombre())
                    .add("dosis", c.getDosis())
                    .add("fecha_aplicacion", Utilidades.ISO8601(c.getFechaAplicacion()))
                    .add("aplicada por", c.getIdProfesional().getPersonas().getApellidoNombre())
                    .add("fecha de carga", Utilidades.ISO8601(c.getFechaCarga()));
            jab.add(job);
        });
        JsonObjectBuilder respuesta = Json.createObjectBuilder().add("status", Json.createObjectBuilder()
                .add("statusCode", 200).add("totalRows", totalRegistros))
                .add("calendario", jab);
        return Response.status(Response.Status.OK).entity(respuesta.build()).build();

    }
    
    
    @Path("/Crear")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(JsonObject calendario) {
        JsonObject obj = calendario.getJsonObject("obj");
        String fecha_carga = obj.getString("fecha_carga");
        int dosis = obj.getInt("dosis");
        String obs = obj.getString("observaciones");
        int id_mascota = obj.getInt("id_mascota");
        int id_vacuna = obj.getInt("id_vacuna");
        String fecha_aplicacion = obj.getString("fecha_aplicacion");
        int id_profesional = obj.getInt("id_profesional");
        
        
        CalendarioVacunas c = new CalendarioVacunas();

        Mascotas m = new Mascotas();
        
        m = mascotaEJB.find(id_mascota);
        if (m != null) {
            c.setIdMascota(m);
        };
        
        Vacunas v = new Vacunas();
        
        v = vacunaEJB.find(id_vacuna);
        if (v != null) {
            c.setIdVacuna(v);
        };
        
        Profesionales pr = new Profesionales();

        Personas p = personaEJB.find(Integer.toUnsignedLong(id_profesional));
        if (p != null) {
            pr.setIdProfesional(p);
        }
        
        
        c.setFechaAplicacion(Utilidades.stringToDate(fecha_aplicacion));
        c.setIdVacuna(v);
        c.setDosis(dosis);
        c.setObservaciones(obs);
        c.setIdMascota(m);
        c.setIdProfesional(pr);
        c.setFechaCarga(Utilidades.stringToDate(fecha_carga));
        
              
        calendarioEJB.create(c);
        
        JsonObjectBuilder job = Json.createObjectBuilder().add("mensaje", "Calendario registrado exitosamente");
        return Response.status(Response.Status.CREATED).entity(job.build()).build();
    }
    
    
}
