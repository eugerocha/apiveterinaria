/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.resources;

import com.euge.apiveterinaria.ejb.ClientesFacadeLocal;
import com.euge.apiveterinaria.entity.Clientes;
import com.euge.apiveterinaria.entity.Personas;
import com.euge.apiveterinaria.utils.Utilidades;
import io.swagger.annotations.Api;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author euge
 */
@Path("/Clientes")
@Api(value = "Clientes")
public class ClientesResource {

    @EJB
    ClientesFacadeLocal clienteEJB;

    @Path("/GetLista")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getListado(
            @DefaultValue("0") @QueryParam("start") int start,
            @DefaultValue("10") @QueryParam("size") int size,
            @DefaultValue("") @QueryParam("search") String search) {

        List<Clientes> lstClientes = clienteEJB.findByParams(start, size, search);

        JsonObjectBuilder job = Json.createObjectBuilder();
        JsonArrayBuilder jab = Json.createArrayBuilder();

        int totalRegistros = clienteEJB.count();
        lstClientes.stream().forEach(c -> {
            job.add("id", c.getPersonas().getId())
                    .add("Apellido y nombre", c.getPersonas().getApellidoNombre())
                    .add("DNI", c.getPersonas().getDni())
                    .add("F.Nacimiento", Utilidades.ISO8601(c.getPersonas().getFnacimiento()))
                    .add("Telefono",c.getPersonas().getTelefono())
                    .add("Correo", c.getPersonas().getEmail())
                    .add("Codigo_cliente", c.getCodigoCliente())
                    .add("estado", c.getEstado());
            jab.add(job);
        });
        JsonObjectBuilder respuesta = Json.createObjectBuilder().add("status", Json.createObjectBuilder()
                .add("statusCode", 200).add("totalRows", totalRegistros))
                .add("clientes", jab);
        return Response.status(Response.Status.OK).entity(respuesta.build()).build();

    }

    @GET
    @Path("/Get")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCliente(@QueryParam("id") Long idcliente) {
        Clientes c = clienteEJB.find(idcliente);
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("status", Json.createObjectBuilder().add("statusCode", 200).add("totalRows", 1))
                .add("cliente", Json.createObjectBuilder()
                        .add("codigo_cliente", c.getCodigoCliente())
                        .add("Apellido y nombre", c.getPersonas().getApellidoNombre())
                        .add("dni", c.getPersonas().getDni())
                        .add("fnacimiento", Utilidades.ISO8601(c.getPersonas().getFnacimiento())));
        return Response.status(Response.Status.OK).entity(builder.build()).build();

    }

    @POST
    @Path("/Find")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response find(@FormParam("dni") String dni) {

        String n = dni;
        System.out.println("Mandaron un..." + n);
        Clientes cli = clienteEJB.findByDni(n);

        JsonObjectBuilder respuesta = Json.createObjectBuilder().add("status", Json.createObjectBuilder()
                .add("statusCode", 200).add("totalRows", "s"))
                .add("infoCliente", Json.createObjectBuilder()
                        .add("id", cli.getPersonas().getId())
                        .add("codigo", cli.getCodigoCliente())
                        .add("apellido y nombre", cli.getPersonas().getApellidoNombre())
                        .add("fnacimiento", Utilidades.ISO8601(cli.getPersonas().getFnacimiento()))
                        .add("dni", cli.getPersonas().getDni()));
        return Response.ok().entity(respuesta.build()).build();
    }

    @Path("/Crear")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(JsonObject c) {

        JsonObject obj = c.getJsonObject("obj");

        String apellido = obj.getString("apellido");
        String nombre = obj.getString("nombre");
        String dni = obj.getString("dni");
        String telefono = obj.getString("telefono");
        String correo = obj.getString("correo");
        String nacimiento = obj.getString("fnacimiento");
        int sexo = obj.getInt("sexo");
        int tipodni = obj.getInt("tipo");

        int cod_cli = obj.getInt("codigo");
        int estado = obj.getInt("estado");

        Personas p = new Personas();

        p.setApellido(apellido);
        p.setNombre(nombre);
        p.setEmail(correo);
        p.setTelefono(telefono);
        p.setDni(dni);
        p.setSexo(sexo);
        p.setTipodni(tipodni);
        p.setFnacimiento(Utilidades.stringToDate(nacimiento));
        p.setFalta(new Date());

        Clientes cli = new Clientes();

        cli.setId(p);
        cli.setCodigoCliente(cod_cli);
        cli.setEstado(estado);

        clienteEJB.create(cli);

        JsonObjectBuilder job = Json.createObjectBuilder().add("mensaje", "Cliente registrado exitosamente");
        return Response.status(Response.Status.CREATED).entity(job.build()).build();
    }
}
