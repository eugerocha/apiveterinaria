/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.resources;

import com.euge.apiveterinaria.ejb.ConsultasFacadeLocal;
import com.euge.apiveterinaria.ejb.MascotasFacadeLocal;
import com.euge.apiveterinaria.ejb.PersonasFacadeLocal;
import com.euge.apiveterinaria.ejb.ProfesionalesFacadeLocal;
import com.euge.apiveterinaria.ejb.VacunasFacadeLocal;
import com.euge.apiveterinaria.entity.Consultas;
import com.euge.apiveterinaria.entity.Mascotas;
import com.euge.apiveterinaria.entity.Personas;
import com.euge.apiveterinaria.entity.Profesionales;
import com.euge.apiveterinaria.utils.Utilidades;
import io.swagger.annotations.Api;
import java.util.List;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author euge
 */

@Path("/Consultas")
@Api(value = "ConsultasMedicas")
public class ConsultasResource {
    
    @EJB 
    ConsultasFacadeLocal consultaEJB;
    
    @EJB
    MascotasFacadeLocal mascotaEJB;
    
    @EJB
    VacunasFacadeLocal vacunaEJB;
    
    @EJB
    ProfesionalesFacadeLocal profesionalEJB;
    
     @EJB
    PersonasFacadeLocal personaEJB;
     
        @Path("/GetLista")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll(@QueryParam("start") int start, @QueryParam("size") int size, @QueryParam("search") String search) {
        List<Consultas> lstConsultas = consultaEJB.findAll();
        JsonObjectBuilder job = Json.createObjectBuilder();
        JsonArrayBuilder jab = Json.createArrayBuilder();
        int totalRegistros = consultaEJB.count();
        lstConsultas.stream().forEach(c -> {
            job.add("id", c.getId())
                .add("fecha", Utilidades.getOnlyDate(c.getFecha()))
                .add("hora", Utilidades.getOnlyHour(c.getHora()))
                .add("motivo",c.getMotivo())
                .add("paciente",c.getIdMascota().getNombre())
                .add("doctor",c.getIdProfesional().getPersonas().getApellidoNombre());
            jab.add(job);
        });
        JsonObjectBuilder respuesta = Json.createObjectBuilder().add("status", Json.createObjectBuilder()
                .add("statusCode", 200).add("totalRows", totalRegistros))
                .add("consulta", jab);
        return Response.status(Response.Status.OK).entity(respuesta.build()).build();

    }
    
    
    @Path("/Crear")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(JsonObject consulta) {
        JsonObject obj = consulta.getJsonObject("obj");
        String fecha = obj.getString("fecha");
        String motivo = obj.getString("motivo");
        int estado = obj.getInt("estado");
        int id_mascota = obj.getInt("id_mascota");
        int id_profesional = obj.getInt("id_profesional");
        
        
        Consultas c = new Consultas();

        Mascotas m = new Mascotas();
        
        m = mascotaEJB.find(id_mascota);
        if (m != null) {
            c.setIdMascota(m);
        };

        
        Profesionales pr = new Profesionales();

        Personas p = personaEJB.find(Integer.toUnsignedLong(id_profesional));
        if (p != null) {
            pr.setIdProfesional(p);
        }
        
        
        c.setFecha(Utilidades.stringToDate(fecha));
        c.setMotivo(motivo);
        c.setEstado(estado);
        c.setIdMascota(m);
        c.setIdProfesional(pr);
        
              
        consultaEJB.create(c);
        
        JsonObjectBuilder job = Json.createObjectBuilder().add("mensaje", "Consulta registrada exitosamente");
        return Response.status(Response.Status.CREATED).entity(job.build()).build();
    }
    
    
    
}
