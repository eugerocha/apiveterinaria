/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.resources;

import com.euge.apiveterinaria.ejb.DiagnosticosFacadeLocal;
import com.euge.apiveterinaria.entity.Diagnosticos;
import com.euge.apiveterinaria.entity.Personas;
import com.euge.apiveterinaria.utils.Utilidades;
import io.swagger.annotations.Api;
import java.util.List;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author euge
 */

@Path("/Diagnosticos")
@Api(value = "Diagnosticos")
public class DiagnosticosResource {
    
    @EJB 
    DiagnosticosFacadeLocal diagnosticoEJB;
    
    @Path("/GetLista")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll(@QueryParam("start") int start, @QueryParam("size") int size, @QueryParam("search") String search) {
        List<Diagnosticos> lstDiagnostico = diagnosticoEJB.findAll();
        JsonObjectBuilder job = Json.createObjectBuilder();
        JsonArrayBuilder jab = Json.createArrayBuilder();
        int totalRegistros = diagnosticoEJB.count();
        lstDiagnostico.stream().forEach(x -> {
            job.add("id", x.getId())
                    .add("diagnostico_primario", x.getDiagnosticoPrimario())
                    .add("afeccion", x.getIdAfeccion().getNombre());
            jab.add(job);
        });
        JsonObjectBuilder respuesta = Json.createObjectBuilder().add("status", Json.createObjectBuilder()
                .add("statusCode", 200).add("totalRows", totalRegistros))
                .add("diagnostico", jab);
        return Response.status(Response.Status.OK).entity(respuesta.build()).build();

    }
    
}
