/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.resources;

//<editor-fold defaultstate="collapsed" desc="imports">
import com.euge.apiveterinaria.ejb.MascotasFacadeLocal;
import com.euge.apiveterinaria.ejb.PersonasFacadeLocal;
import com.euge.apiveterinaria.entity.Clientes;
import com.euge.apiveterinaria.entity.Mascotas;
import com.euge.apiveterinaria.entity.Personas;
import com.euge.apiveterinaria.entity.Razas;
import com.euge.apiveterinaria.entity.TipoMascota;
import com.euge.apiveterinaria.models.MascotaForm;
import com.euge.apiveterinaria.utils.Utilidades;
import com.fcastillo.utilidades.Validacion;
import com.fcastillo.utilidades.excepciones.ErrorMessage;
import com.fcastillo.utilidades.excepciones.NotFoundException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
//</editor-fold>

/**
 *
 * @author euge
 */
@Path("Mascotas")
@Api(value = "Mascotas")
public class MascotasResource {

    @EJB
    MascotasFacadeLocal mascotaEJB;
    @EJB
    PersonasFacadeLocal personaEJB;

    //<editor-fold defaultstate="collapsed" desc="getListado()">
    @Path("/GetLista")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getListado(
            @DefaultValue("0") @QueryParam("start") int start,
            @DefaultValue("10") @QueryParam("size") int size,
            @DefaultValue("") @QueryParam("search") String search) {

        List<Mascotas> lstMascotas = mascotaEJB.findByParams(start, size, search);

        JsonObjectBuilder job = Json.createObjectBuilder();
        JsonArrayBuilder jab = Json.createArrayBuilder();

        int totalRegistros = mascotaEJB.count();
        String path = getPath("mascotas");
        lstMascotas.stream().forEach(m -> {
            String imagen;
            if (m.getImagen().equals("") || m.getImagen() == "" || m.getImagen().isEmpty()) {
                imagen = "avatar.png";
            } else {
                imagen = m.getImagen();
            }
            job.add("id", m.getId())
                    .add("nombre", m.getNombre())
                    .add("imagen", path.concat(imagen))
                    .add("raza", m.getIdRaza().getNombreRaza())
                    .add("sexo", m.getSexo())
                    .add("propietario", m.getIdCliente().getPersonas().getApellidoNombre());
            jab.add(job);
        });
        JsonObjectBuilder respuesta = Json.createObjectBuilder().add("status", Json.createObjectBuilder()
                .add("statusCode", 200).add("totalRows", totalRegistros))
                .add("mascotas", jab);
        return Response.status(Response.Status.OK).entity(respuesta.build()).build();
    }
    //</editor-fold>

    @GET
    @Path("/Get")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMMascota(@QueryParam("id") int idmascota) {
        Mascotas x = mascotaEJB.find(idmascota);
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("status", Json.createObjectBuilder().add("statusCode", 200).add("totalRows", 1))
                .add("mascota", Json.createObjectBuilder()
                        .add("nombre", x.getNombre())
                        .add("tipo", x.getIdTipo().getNombreTipo())
                        .add("raza", x.getIdRaza().getNombreRaza())
                        .add("propietario", x.getIdCliente().getPersonas().getApellidoNombre()));
        return Response.status(Response.Status.OK).entity(builder.build()).build();

    }

    @Path("/Crear")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Registrar una nueva mascota")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Mascota registrada exitosamente"),
        @ApiResponse(code = 400, message = "Bad request"),
        @ApiResponse(code = 500, message = "Ocurrió un error en el servidor")
    })
    public Response crear(@ApiParam(name = "form") MascotaForm mascotaform) {
        Mascotas mascota = new Mascotas();
        Clientes cliente = new Clientes();
        Razas raza = new Razas(mascotaform.getRaza());
        TipoMascota tipoMascota = new TipoMascota(mascotaform.getTipoMascota());

        Personas persona = personaEJB.find(mascotaform.getIdPropietario());

        cliente.setId(persona);

        mascota.setNombre(mascotaform.getNombre());
        mascota.setFechanacimiento(mascotaform.getFnacimiento());
        mascota.setSexo(mascotaform.getSexo());
        mascota.setPeso(mascotaform.getPeso());
        mascota.setAntQuirurgicos(mascotaform.getAntecQuirurgicos());
        mascota.setAntPatologicos(mascotaform.getAntecPatologicos());
        mascota.setAntTraumatologicos(mascotaform.getAntecTraumatologicos());
        mascota.setIdRaza(raza);
        mascota.setIdTipo(tipoMascota);
        mascota.setIdCliente(cliente);

        mascotaEJB.create(mascota);

        JsonObjectBuilder job = Json.createObjectBuilder().add("mensaje",
                Json.createObjectBuilder().add("titulo", "¡Buen trabajo!").add("descripcion", "Se registró exitosamente a " + mascotaform.getNombre()));
        return Response.status(Response.Status.CREATED).entity(job.build()).build();
    }

    @Path("/findCliente")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findCliente(@QueryParam("id") int id) {
        List<Mascotas> lstMascotas = mascotaEJB.findByIdPropietario(id);
        int cantidad = mascotaEJB.count();
        JsonObjectBuilder job = Json.createObjectBuilder();
        JsonArrayBuilder jab = Json.createArrayBuilder();
        lstMascotas.stream().forEach(x -> {
            job.add("id", x.getId());
            job.add("nombre", x.getNombre());
            job.add("sexo", x.getSexo());
            job.add("peso", x.getPeso());
            job.add("fecha nacimiento", Utilidades.ISO8601(x.getFechanacimiento()));
            jab.add(job);
        });
        job.add("statusCode", 200).add("totalRows", cantidad).add("mascotas", jab);
        return Response.status(Response.Status.OK).entity(job.build()).build();
    }

    private String getPath(String sub) {
        ResourceBundle rb = ResourceBundle.getBundle("config");
        String path = rb.getString("host").trim();
        String subdirectory = rb.getString(sub).trim();
        System.out.println(path.concat(subdirectory));
        return path.concat(subdirectory);
    }

    @DELETE
    @Path("Eliminar/{id}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Elimina una mascota")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Mascota eliminada exitosamente"),
        @ApiResponse(code = 404, message = "Recurso no encontrado", response = ErrorMessage.class),
        @ApiResponse(code = 500, message = "Ocurrió un error en el servidor", response = ErrorMessage.class)
    })
    public Response eliminar(@PathParam("id") int id) {
        Mascotas mascota = mascotaEJB.findById(id);

        if (mascota == null) {
            ErrorMessage em = new ErrorMessage(
                    Response.Status.NOT_FOUND,
                    Response.Status.NOT_FOUND.getStatusCode(),
                    "No se encontró ninguna mascota asociada con el id " + id);
            throw new NotFoundException(em);
        }

        mascotaEJB.remove(mascota);
        // FIXME: Se deve mejorar la respuesta con un 200 success
        ErrorMessage em = new ErrorMessage(
                Response.Status.OK,
                Response.Status.OK.getStatusCode(),
                "Se eliminó exitosamente a ".concat(mascota.getNombre()));

        return Response.ok().entity(em).build();
    }

    @PUT
    @Path("Cambiar")
    @ApiOperation(value = "Actulizar información de una mascota")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Mascota editada exitosamente"),
        @ApiResponse(code = 404, message = "Mascota no encontrada", response = ErrorMessage.class),
        @ApiResponse(code = 500, message = "Ocurrió un error en el servidor")
    })
    public Response cambiar(@ApiParam(name = "form") MascotaForm mascotaform) {
        try {
            Mascotas mascota = mascotaEJB.findById(mascotaform.getId());
            Clientes cliente = new Clientes();
            Razas raza = new Razas(mascotaform.getRaza());
            TipoMascota tipoMascota = new TipoMascota(mascotaform.getTipoMascota());
            System.out.println("PROPIETARIO : " + mascota.getIdCliente());
            System.out.println("PErsona : " + mascota.getIdCliente().getId());
            Personas persona = personaEJB.find(mascotaform.getIdPropietario());

            cliente.setId(persona);

            mascota.setNombre(Validacion.defaultValue(mascotaform.getNombre(), mascota.getNombre()));
            mascota.setFechanacimiento(Validacion.defaultValue(mascotaform.getFnacimiento(), mascota.getFechanacimiento()));
            mascota.setSexo(Validacion.defaultValue(mascotaform.getSexo(), mascota.getSexo()));
            mascota.setPeso(Validacion.defaultValue(mascotaform.getPeso(), mascota.getPeso()));
            mascota.setAntQuirurgicos(Validacion.defaultValue(mascotaform.getAntecQuirurgicos(), mascota.getAntQuirurgicos()));
            mascota.setAntPatologicos(Validacion.defaultValue(mascotaform.getAntecPatologicos(), mascota.getAntPatologicos()));
            mascota.setAntTraumatologicos(Validacion.defaultValue(mascotaform.getAntecTraumatologicos(), mascotaform.getAntecTraumatologicos()));
          
           

            mascotaEJB.edit(mascota);
        } catch (Exception e) {
            ErrorMessage em = new ErrorMessage(
                    Response.Status.NOT_FOUND,
                    Response.Status.NOT_FOUND.getStatusCode(),
                    "No se encontró ninguna mascota asociada con el id " + mascotaform.getId());
            throw new NotFoundException(em);
        }

        JsonObjectBuilder job = Json.createObjectBuilder().add("mensaje",
                Json.createObjectBuilder()
                        .add("titulo", "¡Buen trabajo!")
                        .add("descripcion", "Los datos de " + mascotaform.getNombre() + " se actualizaron correctamente"));
        return Response.ok().entity(job.build()).build();
    }

}
