/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.resources;

import com.euge.apiveterinaria.ejb.PersonasFacadeLocal;
import com.euge.apiveterinaria.entity.Personas;
import com.euge.apiveterinaria.utils.Utilidades;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author euge
 */

@Path("Personas")
@Api(value = "Personas")
public class PersonasResource {
    
    @EJB
    PersonasFacadeLocal personaEJB;

    @Path("/GetLista")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Crear un nuevo menu")
    public Response findAll(@QueryParam("start") int start, @QueryParam("size") int size, @QueryParam("search") String search) {
        List<Personas> lstPersonas = personaEJB.findAll();
        JsonObjectBuilder job = Json.createObjectBuilder();
        JsonArrayBuilder jab = Json.createArrayBuilder();
        int totalRegistros = personaEJB.count();
        lstPersonas.stream().forEach(p -> {
            job.add("id", p.getId())
                    .add("nombre", p.getApellidoNombre())
                    .add("DNI", p.getDni())
                    .add("fnacimiento", Utilidades.ISO8601(p.getFnacimiento()));
            jab.add(job);
        });
        JsonObjectBuilder respuesta = Json.createObjectBuilder().add("status", Json.createObjectBuilder()
                .add("statusCode", 200).add("totalRows", totalRegistros))
                .add("personas", jab);
        return Response.status(Response.Status.OK).entity(respuesta.build()).build();

    }
    
    @Path("/Crear")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(JsonObject persona) {
        JsonObject obj = persona.getJsonObject("obj");
        String apellido = obj.getString("apellido");
        String nombre = obj.getString("nombre");
        String dni = obj.getString("dni");
        String telefono = obj.getString("telefono");
        String correo = obj.getString("correo");
        String nacimiento = obj.getString("fnacimiento");
        int sexo = Integer.parseInt(obj.getString("sexo"));
        int tipodni = Integer.parseInt(obj.getString("tipo"));
        
        Personas p = new Personas();

        p.setApellido(apellido);
        p.setNombre(nombre);
        p.setEmail(correo);
        p.setTelefono(telefono);
        p.setDni(dni);
        p.setSexo(sexo);
        p.setTipodni(tipodni);
        p.setFnacimiento(Utilidades.stringToDate(nacimiento));
        p.setFalta(new Date());
        personaEJB.create(p);
        
        JsonObjectBuilder job = Json.createObjectBuilder().add("mensaje", "Persona registrada exitosamente");
        return Response.status(Response.Status.CREATED).entity(job.build()).build();
    }
    
    
    @GET
    @Path("/Get")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPersona(@QueryParam("id") Long idpersona) {
        Personas p = personaEJB.find(idpersona);
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("status", Json.createObjectBuilder().add("statusCode", 200).add("totalRows", 1))
                .add("persona", Json.createObjectBuilder()
                        .add("nombre", p.getApellidoNombre())
                        .add("dni", p.getDni())
                        .add("fnacimiento", Utilidades.ISO8601(p.getFnacimiento()))
                        .add("email", p.getEmail()));
        return Response.status(Response.Status.OK).entity(builder.build()).build();

    }
   
    
    @POST
    @Path("/Find")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response find(@FormParam("dni") String dni) {

        String n = dni;
        System.out.println("Mandaron un..." + n);
        Personas pers = personaEJB.findByDni(n);

        JsonObjectBuilder respuesta = Json.createObjectBuilder().add("status", Json.createObjectBuilder()
                .add("statusCode", 200).add("totalRows", "s"))
                .add("infoPersonal", Json.createObjectBuilder()
                        .add("id", pers.getId())
                        .add("nombre", pers.getApellidoNombre())
                        .add("fnacimiento", Utilidades.ISO8601(pers.getFnacimiento()))
                        .add("dni", pers.getDni())
                        .add("email", pers.getEmail()));
        return Response.ok().entity(respuesta.build()).build();
    }
    
}
