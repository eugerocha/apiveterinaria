/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.resources;

import com.euge.apiveterinaria.ejb.ProfesionalesFacadeLocal;
import com.euge.apiveterinaria.entity.Personas;
import com.euge.apiveterinaria.entity.Profesionales;
import com.euge.apiveterinaria.utils.Utilidades;
import io.swagger.annotations.Api;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author euge
 */
@Path("/Profesionales")
@Api(value = "Medicos")
public class ProfesionalesResource {
    
    @EJB
    ProfesionalesFacadeLocal profesionalEJB;
    
    @Path("/GetLista")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getListado(
            @DefaultValue("0") @QueryParam("start") int start,
            @DefaultValue("10") @QueryParam("size") int size,
            @DefaultValue("") @QueryParam("search") String search) {
        
        List<Profesionales> lstProfesionales = profesionalEJB.findByParams(start, size, search);
        
        JsonObjectBuilder job = Json.createObjectBuilder();
        JsonArrayBuilder jab = Json.createArrayBuilder();
        
        int totalRegistros = profesionalEJB.count();
        String path = getPath("perfil");
        lstProfesionales.stream().forEach(p -> {
            job.add("id", p.getPersonas().getId())
                    .add("imagen", path.concat(p.getPersonas().getImagen()))
                    .add("nombre", p.getPersonas().getApellidoNombre());
            jab.add(job);
        });
        JsonObjectBuilder respuesta = Json.createObjectBuilder().add("status", Json.createObjectBuilder()
                .add("statusCode", 200).add("totalRows", totalRegistros))
                .add("profesionales", jab);
        return Response.status(Response.Status.OK).entity(respuesta.build()).build();
        
    }
    
    @GET
    @Path("/Get")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProfesional(@QueryParam("id") Long id) {
        Profesionales p = profesionalEJB.find(id);
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("status", Json.createObjectBuilder().add("statusCode", 200).add("totalRows", 1))
                .add("profesional", Json.createObjectBuilder()
                        .add("nombre", p.getPersonas().getApellidoNombre())
                        .add("dni", p.getPersonas().getDni())
                        .add("fnacimiento", Utilidades.ISO8601(p.getPersonas().getFnacimiento()))
                        .add("email", p.getPersonas().getEmail()));
        return Response.status(Response.Status.OK).entity(builder.build()).build();
        
    }
    
    @POST
    @Path("/Find")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response find(@FormParam("dni") String dni) {
        
        String n = dni;
        System.out.println("Mandaron un..." + n);
        Profesionales pro = profesionalEJB.findByDni(n);
        
        JsonObjectBuilder respuesta = Json.createObjectBuilder().add("status", Json.createObjectBuilder()
                .add("statusCode", 200).add("totalRows", "s"))
                .add("infoVeterinario", Json.createObjectBuilder()
                        .add("id", pro.getPersonas().getId())
                        .add("legajo", pro.getLegajo())
                        .add("nombre", pro.getPersonas().getApellidoNombre())
                        .add("fnacimiento", Utilidades.ISO8601(pro.getPersonas().getFnacimiento()))
                        .add("dni", pro.getPersonas().getDni())
                        .add("email", pro.getPersonas().getEmail()));
        return Response.ok().entity(respuesta.build()).build();
    }
    
    @Path("/Crear")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(JsonObject pro) {
        
        JsonObject obj = pro.getJsonObject("obj");
        
        String apellido = obj.getString("apellido");
        String nombre = obj.getString("nombre");
        String dni = obj.getString("dni");
        String telefono = obj.getString("telefono");
        String correo = obj.getString("correo");
        String nacimiento = obj.getString("fnacimiento");
        int sexo = obj.getInt("sexo");
        int tipodni = Integer.parseInt(obj.getString("tipo"));
        
        String legajo = obj.getString("legajo");
        int estado = Integer.parseInt(obj.getString("estado"));
        
        Personas p = new Personas();
        
        p.setApellido(apellido);
        p.setNombre(nombre);
        p.setEmail(correo);
        p.setTelefono(telefono);
        p.setDni(dni);
        p.setSexo(sexo);
        p.setTipodni(tipodni);
        p.setFnacimiento(Utilidades.stringToDate(nacimiento));
        p.setFalta(new Date());
        
        Profesionales pr = new Profesionales();
        
        pr.setLegajo(legajo);
        pr.setEstado(estado);
        
        profesionalEJB.create(pr);
        
        JsonObjectBuilder job = Json.createObjectBuilder().add("mensaje", "Profesional registrado exitosamente");
        return Response.status(Response.Status.CREATED).entity(job.build()).build();
    }
    
    private String getPath(String sub) {
        ResourceBundle rb = ResourceBundle.getBundle("config");
        String path = rb.getString("host").trim();
        String subdirectory = rb.getString(sub).trim();
        System.out.println(path.concat(subdirectory));
        return path.concat(subdirectory);
    }
}
