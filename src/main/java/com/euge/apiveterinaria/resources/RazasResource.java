/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.resources;

import com.euge.apiveterinaria.ejb.RazasFacadeLocal;
import com.euge.apiveterinaria.entity.Razas;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author euge
 */

@Path("/Razas")
@Api(value = "Razas")
public class RazasResource {
    
    @EJB
    RazasFacadeLocal razasEJB;
    
//    @Path("/GetLista")
//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response findAll(@QueryParam("start") int start, @QueryParam("size") int size, @QueryParam("search") String search) {
//        List<Razas> lstRazas = razasEJB.findAll();
//        JsonObjectBuilder job = Json.createObjectBuilder();
//        JsonArrayBuilder jab = Json.createArrayBuilder();
//        int totalRegistros = razasEJB.count();
//        lstRazas.stream().forEach(r -> {
//            job.add("id", r.getId())
//                .add("nombre", r.getNombreRaza())
//                .add("tipo", r.getTipo().getNombreTipo());
//            jab.add(job);
//        });
//        JsonObjectBuilder respuesta = Json.createObjectBuilder().add("status", Json.createObjectBuilder()
//                .add("statusCode", 200).add("totalRows", totalRegistros))
//                .add("raza", jab);
//        return Response.status(Response.Status.OK).entity(respuesta.build()).build();
//
//    }
    
    
    @GET
    @Path("/Get")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get tipo")
    public Response getTipo(@ApiParam(value = "id") @QueryParam("id") int id) {
        Razas r = razasEJB.find(id);
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("status", Json.createObjectBuilder().add("statusCode", 200).add("totalRows", 1))
                .add("raza", Json.createObjectBuilder()
                        .add("id", r.getId())
                        .add("nombre", r.getNombreRaza())
                        .add("tipo", r.getTipo().getNombreTipo()));
        return Response.status(Response.Status.OK).entity(builder.build()).build();

    }
    
    
       
    @Path("/Crear")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(JsonObject raza) {
        JsonObject obj = raza.getJsonObject("obj");
        String nombre_raza = obj.getString("nombre_tipo");
        int tipo = Integer.parseInt(obj.getString("tipo"));
        
        Razas r= new Razas();

        r.setNombreRaza(nombre_raza);  
       
       
        razasEJB.create(r);
        
        JsonObjectBuilder job = Json.createObjectBuilder().add("mensaje", "Raza registrada exitosamente");
        return Response.status(Response.Status.CREATED).entity(job.build()).build();
    }
    
    
     @GET
    @Path("/GetList")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response find(@QueryParam("search") String search) {
        List<Razas> lstRazas = new ArrayList<>();
        JsonObjectBuilder job = Json.createObjectBuilder();
        JsonArrayBuilder jab = Json.createArrayBuilder();
        search = search.trim();
        lstRazas = razasEJB.findByNameLike(search.toLowerCase());
        lstRazas.stream().forEach(r -> {
            job.add("id", r.getId() ).add("nombre", r.getNombreRaza());
            jab.add(job);
        });
        JsonObjectBuilder respuesta = Json.createObjectBuilder();
        respuesta.add("razas", jab);
        return Response.status(Response.Status.OK).entity(respuesta.build()).build();

    }
    
    @Path("/GetRazas")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRazas(@QueryParam("id") int id)  {
        List<Razas> lstRazas = razasEJB.findByTipoMascota(id);
        int cantidad = razasEJB.count();
        JsonObjectBuilder job = Json.createObjectBuilder();
        JsonArrayBuilder jab = Json.createArrayBuilder();
        lstRazas.stream().forEach(x -> {
            job.add("id", x.getId());
            job.add("nombre", x.getNombreRaza());
            job.add("Tipo", x.getTipo().getNombreTipo());
            jab.add(job);
        });
        job.add("statusCode", 200).add("totalRows", cantidad).add("razas", jab);
        return Response.status(Response.Status.OK).entity(job.build()).build();
    }
    
}
