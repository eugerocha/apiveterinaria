/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.resources;

import com.euge.apiveterinaria.ejb.TipoMascotaFacadeLocal;
import com.euge.apiveterinaria.entity.TipoMascota;
import io.swagger.annotations.Api;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author euge
 */

@Path ("/TipoMascota")
@Api(value = "TipoMascota")
public class TipoMascotaResource {

@EJB 
TipoMascotaFacadeLocal tipoMascotaEJB;  

@Path("/GetLista")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll(@QueryParam("start") int start, @QueryParam("size") int size, @QueryParam("search") String search) {
        List<TipoMascota> lstTipo = tipoMascotaEJB.findAll();
        JsonObjectBuilder job = Json.createObjectBuilder();
        JsonArrayBuilder jab = Json.createArrayBuilder();
        int totalRegistros = tipoMascotaEJB.count();
        lstTipo.stream().forEach(t -> {
            job.add("id", t.getId())
                .add("tipo", t.getNombreTipo());
            jab.add(job);
        });
        JsonObjectBuilder respuesta = Json.createObjectBuilder().add("status", Json.createObjectBuilder()
                .add("statusCode", 200).add("totalRows", totalRegistros))
                .add("tipoMascota", jab);
        return Response.status(Response.Status.OK).entity(respuesta.build()).build();

    }
    
    
     @GET
    @Path("/Get")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTipo(@QueryParam("id") int id) {
        TipoMascota t = tipoMascotaEJB.find(id);
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("status", Json.createObjectBuilder().add("statusCode", 200).add("totalRows", 1))
                .add("tipoMascota", Json.createObjectBuilder()
                        .add("id", t.getId())
                        .add("tipo", t.getNombreTipo()));
        return Response.status(Response.Status.OK).entity(builder.build()).build();

    }

    
    @Path("/Crear")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(JsonObject tipoMascota) {
        JsonObject obj = tipoMascota.getJsonObject("obj");
        String nombre_tipo = obj.getString("nombre_tipo");
        
        TipoMascota t= new TipoMascota();

        t.setNombreTipo(nombre_tipo);   
       
        tipoMascotaEJB.create(t);
        
        JsonObjectBuilder job = Json.createObjectBuilder().add("mensaje", "Tipo de mnascota registrada exitosamente");
        return Response.status(Response.Status.CREATED).entity(job.build()).build();
    }
    
     @GET
    @Path("/Find")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response find(@QueryParam("search") String search) {
        List<TipoMascota> lstTipo = new ArrayList<>();
        JsonObjectBuilder job = Json.createObjectBuilder();
        JsonArrayBuilder jab = Json.createArrayBuilder();
        search = search.trim();
        lstTipo = tipoMascotaEJB.findByNameLike(search.toLowerCase());
        lstTipo.stream().forEach(r -> {
            job.add("id", r.getId() ).add("nombre", r.getNombreTipo());
            jab.add(job);
        });
        JsonObjectBuilder respuesta = Json.createObjectBuilder();
        respuesta.add("razas", jab);
        return Response.status(Response.Status.OK).entity(respuesta.build()).build();

    }
}
