/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.resources;

import com.euge.apiveterinaria.ejb.MascotasFacadeLocal;
import com.euge.apiveterinaria.ejb.PersonasFacadeLocal;
import com.euge.apiveterinaria.ejb.TurnosFacadeLocal;
import com.euge.apiveterinaria.entity.Mascotas;
import com.euge.apiveterinaria.entity.Personas;
import com.euge.apiveterinaria.entity.Profesionales;
import com.euge.apiveterinaria.entity.Turnos;
import io.swagger.annotations.Api;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author euge
 */
@Path("/Turnos")
@Api(value = "Turnos")
public class TurnosResource {

    @EJB
    TurnosFacadeLocal turnosEJB;
    @EJB
    MascotasFacadeLocal mascotaEJB;
    @EJB
    PersonasFacadeLocal personaEJB;

    @Path("/GetLista")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response findAll(
            @DefaultValue("0") @QueryParam("start") int start,
            @DefaultValue("10") @QueryParam("size") int size,
            @DefaultValue("") @QueryParam("search") String search) {

        List<Turnos> lstTurnos = turnosEJB.findByParams(start, size, search);
        JsonObjectBuilder job = Json.createObjectBuilder();
        JsonArrayBuilder jab = Json.createArrayBuilder();
        int totalRegistros = turnosEJB.count();
        lstTurnos.stream().forEach(x -> {
            job.add("id", x.getId())
                    .add("fechaTurno", x.getFecha().toInstant().toString())
                    .add("nombre_mascota", x.getIdMascota().getNombre())
                    .add("propietario", x.getIdMascota().getIdCliente().getPersonas().getApellidoNombre())
                    .add("veterinario", x.getIdProfesional().getPersonas().getApellidoNombre());
            jab.add(job);
        });
        JsonObjectBuilder respuesta = Json.createObjectBuilder().add("status", Json.createObjectBuilder()
                .add("statusCode", 200).add("totalRows", totalRegistros))
                .add("turnos", jab);
        return Response.status(Response.Status.OK).entity(respuesta.build()).build();

    }

    @Path("/Crear")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(JsonObject turno) {
        JsonObject obj = turno.getJsonObject("obj");
        int idmascota = obj.getInt("id_mascota");
        int idvet = obj.getInt("idvet");

        Turnos t = new Turnos();

        Mascotas m = mascotaEJB.find(idmascota);

        if (m != null) {
            t.setIdMascota(m);
        }

        Profesionales pro = new Profesionales();

        Personas p = personaEJB.find(Integer.toUnsignedLong(idvet));
        if (p != null) {
            pro.setIdProfesional(p);
        }
        t.setFecha(new Date());

        t.setIdMascota(m);
        t.setIdProfesional(pro);

        turnosEJB.create(t);

        JsonObjectBuilder job = Json.createObjectBuilder().add("mensaje",
                Json.createObjectBuilder().add("titulo", "¡Buen trabajo!").add("descripcion", "Se registró exitosamente "));
        return Response.status(Response.Status.CREATED).entity(job.build()).build();
    }

}
