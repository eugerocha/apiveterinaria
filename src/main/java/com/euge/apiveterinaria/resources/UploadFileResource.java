/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.resources;

import com.euge.apiveterinaria.ejb.MascotasFacadeLocal;
import com.euge.apiveterinaria.entity.Mascotas;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotEmpty;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 *
 * @author fcastillo
 */
@Path("/Files")
public class UploadFileResource {

    private final String UPLOAD_FILE_PATH = "/home/fcastillo/uploads/";
    @EJB
    MascotasFacadeLocal mascotaejb;

    @POST
    @Path("/Upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) {

        String uploadedFileLocation = "/home/fcastillo/upload/" + fileDetail.getFileName();

        // save it
        writeToFile(uploadedInputStream, uploadedFileLocation);
        Mascotas m = mascotaejb.find(1);
        m.setImagen(fileDetail.getName());
        mascotaejb.edit(m);

        String output = "File uploaded to : " + uploadedFileLocation;

        return Response.status(200).entity(output).build();

    }

    // save uploaded file to new location
    private void writeToFile(InputStream uploadedInputStream,
            String uploadedFileLocation) {

        try {
            OutputStream out = new FileOutputStream(new File(
                    uploadedFileLocation));
            int read = 0;
            byte[] bytes = new byte[1024];

            out = new FileOutputStream(new File(uploadedFileLocation));
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();
        } catch (IOException e) {

            e.printStackTrace();
        }

    }

    @GET
    @Path("/{subdirectory}/{filename}")
    @Produces("image/png")
    public Response getFile(@PathParam("subdirectory") String subdirectory, @PathParam("filename") String filename) {
        String rootDirectory = getRootDirectory();
        String path = rootDirectory.concat(subdirectory).concat("/").concat(filename);

        File file = new File(path);
        ResponseBuilder response = Response.ok((Object) file);

        response.header(
                "Content-Disposition",
                "inline; filename=" + file.getName());
        return response.build();

    }

    private String getRootDirectory() {
        ResourceBundle rb = ResourceBundle.getBundle("config");
        String directory = rb.getString("uploaddirectory").trim();
        return directory;
    }
}
