/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.resources;

import com.euge.apiveterinaria.ejb.PersonasFacadeLocal;
import com.euge.apiveterinaria.ejb.UsuariosFacadeLocal;
import com.euge.apiveterinaria.entity.Personas;
import com.euge.apiveterinaria.entity.Usuarios;
import com.euge.apiveterinaria.models.Credentials;
import com.euge.apiveterinaria.utils.TokenIssuer;
import com.euge.apiveterinaria.utils.Utilidades;
import io.swagger.annotations.Api;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author euge
 */


@Path("/Usuarios")
public class UsuarioResource {
    
    @EJB
    UsuariosFacadeLocal usuarioEJB;
    @EJB
    PersonasFacadeLocal personaEJB;
    @Context
    ResourceContext ctx;

    @GET
    @Path("/Cantidad")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTotalOfRows() {
        int cantidad = usuarioEJB.count();
        JsonObjectBuilder respuesta = Json.createObjectBuilder().add("status",
                Json.createObjectBuilder().add("statusCode", 200)
                        .add("totalRows", cantidad));
        return Response.status(Response.Status.OK).entity(respuesta.build()).build();
    }

    @POST
    @Path("/Crear")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(@FormParam("idpersona") int idpersona, @FormParam("username") String username, @FormParam("password") String password) {

        Usuarios usuario = new Usuarios();
        usuario.setUsername(username);
        usuario.setPassword(password);
        usuario.setEstado(1);
        usuario.setFechaAlta(new Date());

        usuarioEJB.create(usuario);
        return Response.status(Response.Status.CREATED).build();
    }

    @POST
    @Path("/Authenticate")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response authenticate(@BeanParam Credentials credenciales) {

        Usuarios usuario = usuarioEJB.findyByUserName(credenciales.getUsername());

        JsonObjectBuilder job = Json.createObjectBuilder();
        if (usuario == null) {
            job.add("error", "Nombre de usuario y/o password incorrectos");
            return Response.status(Response.Status.NOT_FOUND).entity(job.build()).build();
        }
        String token = TokenIssuer.generarToken(usuario.getUsername());

        job = Json.createObjectBuilder()
                .add("token", token)
                .add("tokenType", "Bearer")
                .add("aspellido", usuario.getPersonas().getApellido())
                .add("userName", usuario.getUsername())
                .add("id", usuario.getPersonas().getId());
        return Response.status(Response.Status.OK).entity(job.build()).build();
    }

    @POST
    @Path("/Find")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response findByUsername(@FormParam("username") String username) {
        Usuarios usuario = usuarioEJB.findyByUserName(username);

        JsonObjectBuilder respuesta = Json.createObjectBuilder();
        if (usuario != null) {
            respuesta.add("error", "Ya se encuentra registrada una cuenta de usuario con este DNI");
            return Response.status(Response.Status.CONFLICT).entity(respuesta.build()).build();
        }
        Personas pers = personaEJB.findByDni(username);
        if (pers == null) {
            respuesta.add("error", "No se encontró ninguna persona registrada con este DNI   ");

            return Response.status(Response.Status.NOT_FOUND).entity(respuesta.build()).build();
        }
        respuesta = Json.createObjectBuilder().add("status", Json.createObjectBuilder()
                .add("statusCode", 200).add("totalRows", 1))
                .add("infoPersonal", Json.createObjectBuilder()
                        .add("id", pers.getId())
                        .add("nombre", pers.getApellidoNombre())
                        .add("dni", pers.getDni()).add("email", pers.getEmail()));
        return Response.status(Response.Status.OK).entity(respuesta.build()).build();
    }

    @GET
    @Path("/GetLista")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response findByParameters(@QueryParam("start") int start, @QueryParam("size") int size) {
        List<Usuarios> lstUsuarios = usuarioEJB.getPaginated(start, size);
        int totalRows = usuarioEJB.count();
        JsonObjectBuilder builder = Json.createObjectBuilder();
        JsonArrayBuilder jab = Json.createArrayBuilder();
        for (Usuarios u : lstUsuarios) {
            builder.add("username", u.getUsername())
                    .add("email", u.getPersonas().getEmail())
                    .add("img", "../images/castillo.jpg")
                    .add("fcreacion", Utilidades.ISO8601(u.getFechaAlta()))
                    .add("estado", u.getEstado())
                    .add("ultimoLogin", Utilidades.ISO8601(u.getUltimoLogin()));
            jab.add(builder);
        }
        builder.add("status", Json.createObjectBuilder().add("statusCode", 200).add("totalRows", totalRows));
        builder.add("usuarios", jab);

        return Response.status(Response.Status.OK).entity(builder.build()).build();

    }

    @GET
    @Path("/{userId}")
    public Usuarios getUsuario(@PathParam("userId") int id) {
        return usuarioEJB.find(id);
    } 
}


