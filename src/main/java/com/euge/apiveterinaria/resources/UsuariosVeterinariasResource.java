/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.resources;

import com.euge.apiveterinaria.ejb.UsuariosVeterinariasFacadeLocal;
import com.euge.apiveterinaria.entity.UsuariosVeterinarias;
import java.util.List;
import javax.ejb.EJB;
import static javax.enterprise.deploy.shared.ModuleType.EJB;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author euge
 */

@Path("/UsuariosVeterinarias")
public class UsuariosVeterinariasResource {
    
    @EJB
    UsuariosVeterinariasFacadeLocal usuariosVetEJB;
    
    @Path("/GetLista")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll(@QueryParam("start") int start, @QueryParam("size") int size, @QueryParam("search") String search) {
        List<UsuariosVeterinarias> lstUsuarios = usuariosVetEJB.findAll();
        JsonObjectBuilder job = Json.createObjectBuilder();
        JsonArrayBuilder jab = Json.createArrayBuilder();
        int totalRegistros = usuariosVetEJB.count();
        lstUsuarios.stream().forEach(u -> {
            job.add("id_usuario", u.getUsuarios().getPersonas().getId())
                    .add("id_veterinaria", u.getVeterinarias().getId())
                    .add("id_rol", u.getRoles().getId());
                    
            jab.add(job);
        });
        JsonObjectBuilder respuesta = Json.createObjectBuilder().add("status", Json.createObjectBuilder()
                .add("statusCode", 200).add("totalRows", totalRegistros))
                .add("usuariosVeterinarias", jab);
        return Response.status(Response.Status.OK).entity(respuesta.build()).build();

    }
    
}
