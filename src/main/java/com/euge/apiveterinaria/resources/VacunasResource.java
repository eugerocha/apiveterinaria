/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.resources;

import com.euge.apiveterinaria.ejb.VacunasFacadeLocal;
import com.euge.apiveterinaria.entity.Vacunas;
import io.swagger.annotations.Api;
import java.util.List;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author euge
 */

@Path("/Vacunas")
@Api(value = "Vacunas")
public class VacunasResource {
    
    @EJB 
    VacunasFacadeLocal vacunaEJB;
    
    
   @Path("/GetLista")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getListado(
            @DefaultValue("0") @QueryParam("start") int start,
            @DefaultValue("10") @QueryParam("size") int size,
            @DefaultValue("") @QueryParam("search") String search) {

        List<Vacunas> lstVacunas = vacunaEJB.findByParams(start, size, search);

        JsonObjectBuilder job = Json.createObjectBuilder();
        JsonArrayBuilder jab = Json.createArrayBuilder();

        int totalRegistros = vacunaEJB.count();
        lstVacunas.stream().forEach(x -> {
            job.add("id", x.getId())
                    .add("nombre", x.getNombre())
                    .add("edad aplicación", x.getEdadAplicacion());
            jab.add(job);
        });
        JsonObjectBuilder respuesta = Json.createObjectBuilder().add("status", Json.createObjectBuilder()
                .add("statusCode", 200).add("totalRows", totalRegistros))
                .add("vacunas", jab);
        return Response.status(Response.Status.OK).entity(respuesta.build()).build();

    }
    
     @Path("/GetVacunas")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVacunas(@QueryParam("id") int id)  {
        List<Vacunas> lstVacunas = vacunaEJB.findByTipoMascota(id);
        int cantidad = vacunaEJB.count();
        JsonObjectBuilder job = Json.createObjectBuilder();
        JsonArrayBuilder jab = Json.createArrayBuilder();
        lstVacunas.stream().forEach(x -> {
            job.add("id", x.getId());
            job.add("nombre", x.getNombre());
            job.add("edad_aplicacion", x.getEdadAplicacion());
            job.add("detalle", x.getDetalle());
            jab.add(job);
        });
        job.add("statusCode", 200).add("totalRows", cantidad).add("vacunas", jab);
        return Response.status(Response.Status.OK).entity(job.build()).build();
    }
    
    
    @Path("/Crear")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(JsonObject vacuna) {
        JsonObject obj = vacuna.getJsonObject("obj");
        String nombre = obj.getString("nombre");
        String edad_aplicacion = obj.getString("edad_aplicacion");
        
        Vacunas v = new Vacunas();

        v.setNombre(nombre);
        v.setEdadAplicacion(edad_aplicacion);
        
                
        vacunaEJB.create(v);
        
        JsonObjectBuilder job = Json.createObjectBuilder().add("mensaje", "Vacuna registrada exitosamente");
        return Response.status(Response.Status.CREATED).entity(job.build()).build();
    }
    
    
   
    
}
