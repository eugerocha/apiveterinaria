/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.resources;

import com.euge.apiveterinaria.ejb.VeterinariasFacadeLocal;
import com.euge.apiveterinaria.entity.Veterinarias;
import com.fcastillo.utilidades.Validacion;
import com.fcastillo.utilidades.excepciones.ErrorMessage;
import com.fcastillo.utilidades.excepciones.NotFoundException;
import io.swagger.annotations.Api;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author euge
 */
@Path("/Veterinarias")
@Api(value = "Veterinarias")
public class VeterinariasResource {

    @EJB
    VeterinariasFacadeLocal veterinariaEJB;

    @Path("/GetLista")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll(@QueryParam("start") int start, @QueryParam("size") int size, @QueryParam("search") String search) {
        List<Veterinarias> lstVeterinarias = veterinariaEJB.findAll();

        if (lstVeterinarias.isEmpty()) {
            ErrorMessage em = new ErrorMessage(Response.Status.NOT_FOUND, Response.Status.NOT_FOUND.getStatusCode(), "Listado vacio");
            throw new NotFoundException(em);
        }

        JsonArrayBuilder jab = Json.createArrayBuilder();
        JsonArrayBuilder horariosAtencion = Json.createArrayBuilder();

        int totalRegistros = veterinariaEJB.count();

        String path = getPath("veterinarias");

        lstVeterinarias.stream().forEach(x -> {
            String linkImagen = Optional.ofNullable(x.getImagen()).orElse("avatar.png");
            // Horarios atencion por cada veterinaria
            x.getHorariosVeterinariasCollection().forEach(h -> {
                horariosAtencion.add(Json.createObjectBuilder()
                        .add("veterinaria", x.getRazonSocial())
                        .add("id", h.getId())
                        .add("dia", h.getDia())
                        .add("apertura", h.getApertura().toString())
                        .add("cierre", h.getCierre().toString())
                        .add("observaciones", h.getObservaciones()));
            });

            // Informacion general de cada veterinaria
            jab.add(Json.createObjectBuilder()
                    .add("id", x.getId())
                    .add("razonSocial", Validacion.defaultValue(x.getRazonSocial(), "No disponible"))
                    .add("cuit", Validacion.defaultValue(x.getCuit(), "No disponible"))
                    .add("domicilio", Validacion.defaultValue(x.getDomicilio(), "No disponible"))
                    .add("telefono", Validacion.defaultValue(x.getTelefono(), "No disponible"))
                    .add("correo", Validacion.defaultValue(x.getEmail(), "No disponible"))
                    .add("imagen", path.concat(linkImagen))
                    .add("estado", Validacion.defaultValue(x.getEstado(), -1))
                    .add("horarios", horariosAtencion));
        });

        JsonObjectBuilder respuesta = Json.createObjectBuilder().add("status", Json.createObjectBuilder()
                .add("statusCode", 200).add("totalRows", totalRegistros))
                .add("veterinarias", jab);

        return Response.status(Response.Status.OK).entity(respuesta.build()).build();

    }

    @Path("/Crear")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(JsonObject veterinaria) {
        JsonObject obj = veterinaria.getJsonObject("obj");
        String razonSocial = obj.getString("razonSocial");
        String domicilio = obj.getString("domicilio");
        String cuit = obj.getString("cuit");
        String telefono = obj.getString("telefono");
        String correo = obj.getString("correo");
        String encargado = obj.getString("encargado");
        int estado = Integer.parseInt(obj.getString("estado"));

        Veterinarias v = new Veterinarias();

        v.setRazonSocial(razonSocial);
        v.setDomicilio(domicilio);
        v.setCuit(cuit);
        v.setTelefono(telefono);
        v.setEmail(correo);
        v.setEncargado(encargado);
        v.setEstado(estado);

        veterinariaEJB.create(v);

        JsonObjectBuilder job = Json.createObjectBuilder().add("mensaje", "Veterinaria registrada exitosamente");
        return Response.status(Response.Status.CREATED).entity(job.build()).build();
    }

    private String getPath(String sub) {
        ResourceBundle rb = ResourceBundle.getBundle("config");
        String path = rb.getString("host").trim();
        String subdirectory = rb.getString(sub).trim();
        return path.concat(subdirectory);
    }

    
     @Path("/Eliminar")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminar(@QueryParam("id") int id) {
        Veterinarias v = veterinariaEJB.findById(id);

        veterinariaEJB.remove(v);

        JsonObjectBuilder job = Json.createObjectBuilder().add("mensaje", "Veterinaria eliminada exitosamente");
        return Response.status(Response.Status.CREATED).entity(job.build()).build();
    }
}
