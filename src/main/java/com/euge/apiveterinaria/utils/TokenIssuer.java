/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euge.apiveterinaria.utils;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author fcastillo
 */
public class TokenIssuer {

    public static final long MINUTOS_EXPIRACION = 60L;

    public static String generarToken(String username) {
        LocalDateTime periodoExpiracion = LocalDateTime.now().plusMinutes(MINUTOS_EXPIRACION);
        Date horaExpiracion = Date.from(periodoExpiracion.atZone(ZoneId.systemDefault()).toInstant());
        Key key = new SecretKeySpec("secret".getBytes(), "DES");
        String JWSCompact = Jwts.builder()
                .setSubject(username)
                .signWith(SignatureAlgorithm.HS256, key)
                .setIssuedAt(new Date())
                .setExpiration(horaExpiracion)
                .compact();
        return JWSCompact;
    }
}
