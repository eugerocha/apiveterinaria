/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  euge
 * Created: 25/02/2020
 */
/*Carga tipos de mascota*/
INSERT INTO TipoMascota (nombre_tipo) VALUES
("Hámster"),
("Ave"),
("Pez"),
("Conejo"),
("Hurón"),
("Cobayo"),
("Erizo"),
("Ratón"),
("Tortuga"),
("Iguana"),
("Serpiente");


/*Carga razas de perro*/
INSERT INTO Razas (nombre_raza, tipo) VALUES 
("Coker",1),
("Mestizo",1),
("Husky siberiano",1),
("Golden retriever",1),
("Caniche",1),
("Pastor alemán",1),
("Dálmata",1),
("Boxer",1),
("Chihuahua",1),
("Beagle",1),
("Bulldog",1),
("Bulldog francés",1),
("Rottwailer",1),
("Yorkshire terrier",1),
("Chow chow",1),
("Sharpei",1),
("Pekinés",1),
("Galgo",1),
("Weimaraner",1),
("Doberman",1),
("Galgo",1),
("Setter irlandés",1),
("Collie",1),
("Border collie",1),
("Labrador",1),
("Dachshund",1),
("Pug",1),
("Fila brasilero",1),
("Mastin napolitano",1),
("Dogo argentino",1),
("Dogo de Burdeos",1),
("Pitbull",1),
("Shih tzu",1),
("Basset hound",1),
("Bull terrier",1),
("Fox terrier",1),
("Bichón frisé",1),
("Maltés",1),
("Pastor belga",1),
("Pinscher",1),
("Gran danés",1),
("Pointer",1),
("Schnauzer",1),
("Basset hound",1);


/*Carga razas de gato*/
INSERT INTO Razas (nombre_raza, tipo) VALUES 
("Bengalí",2),
("Maine coon",2),
("Mau egipcio",2),
("Esfinge",2),
("Persa",2),
("Angora",2);


/*Carga razas de hamster*/
INSERT INTO Razas (nombre_raza, tipo) VALUES 
("Ruso",3),
("Sirio o dorado",3),
("Chino",3),
("Enano de Campbell",3),
("de Roborowski",3);


/*Carga razas de aves*/
INSERT INTO Razas (nombre_raza, tipo) VALUES 
("Loro",4),
("Perico",4),
("Canario",4),
("Guacamayo",4),
("Gallina",4),
("Pato",4),
("Jilguero",4),
("Cacatúa",4);

/*Carga razas de pez*/
INSERT INTO Razas (nombre_raza, tipo) VALUES
("Koi",5),
("Angel",5),
("Dorado",5),
("Payaso",5),
("Betta",5),
("Ojo burbuja",5),
("Plata",5),
("Cebra",5),
("Tetra neon",5),
("Molly",5),
("Ramirezi",5);

/*Carga razas de conejo*/
INSERT INTO Razas (nombre_raza, tipo) VALUES
("Rex",6),
("Cabeza de león",6),
("Holandés",6),
("Angora",6),
("Arlequín",6),
("Californiano",6);


/*Carga razas cobayo*/
INSERT INTO Razas (nombre_raza, tipo) VALUES
("Peruano",8),
("Skinny",8),
("Americana",8),
("Teddy",8),
("Himalaya",8),
("Holandés",8);

/*Carga razas erizo*/
INSERT INTO Razas (nombre_raza, tipo) VALUES
("Egipcio",9),
("Europeo",9),
("Sudafricano",9),
("Somalí",9),
("de vientre blanco",9),
("de los Balcanes",9);

/*Carga razas de ratón*/
INSERT INTO Razas (nombre_raza, tipo) VALUES
("común",10),
("Manx",10),
("Calva",10),
("Dumbo",10),
("Rex",10),
("Chino",10);

/*Carga razas de tortuga*/
INSERT INTO Razas (nombre_raza, tipo) VALUES
("Orejas rojas",11),
("Orejas amarrillas",11),
("del bosque",11),
("Mora",11);

/*Raza sin especificar de algunos tipos de mascota*/
INSERT INTO Razas (nombre_raza, tipo) VALUES
("sin especificar",7),
("sin especificar",12),
("sin especificar",13);

/*Carga datos de afecciones*/
INSERT INTO Afecciones() VALUES(1,"Otitis","inflamación del oído");

INSERT INTO Afecciones() VALUES
(2,"Moquillo","tos, estornudos, secreciones, fiebre, diarrea y tics nerviosos"),
(3,"Sarna","prurito, alopecia e inflamación"),
(4,"Parásitos internos","tenias en las heces"),
(5,"Artrosis","inflamación,degeneración de articulaciones, principalmente cadera y codo"),
(6,"Parvovirus","vómitos,falta de apetito,heces con sangre,cansancio,diarrea,falta de energía,deshidratación"),
(7,"Gastritis","vómitos,distensión abdominal,pérdida de apetito o peso,falta de energía,producción excesiva de saliva"),
(8,"Leishmaniosis","adelgazamiento,fiebre,anemia,artritis"),
(9,"Hepatitis","sed excesiva,ictericia,dolor abdominal,fiebre,convulsiones por fallo hepático,pérdida del apetito,vómitos"),
(10,"Leptospirosis","fiebre,diarrea,vómitos con sangre,orina oscura"),
(11,"Rabia","violencia extrema sin provocación alguna"),
(12,"Traqueobronquitis infecciosa","tos seca y áspera"),
(13,"Coronavirus","vómitos,diarrea,pérdida de peso"),
(14,"Toxoplasmosis","síntomas neuromusculares,respiratorios,gastrointestinales"),
(15,"Conjuntivitis","enrojecimiento ocular, secreciones"),
(16,"Cancer","tumores en la piel,pueden desarrollarse también de forma interna"),
(17,"Glaucoma","acumulación excesiva de líquido en el ojo"),
(18,"Reumatismo","rigidez, inflamación y dolor"),
(19,"Displasia de cadera","cojera o dificultad de movimientos"),
(20,"Epilepsia","cojera o dificultad de movimientos"),
(21,"Sindrome vestibular","cabeza torcida o ladeada,pérdida de equilibrio,mareos,vértigos,náuseas,babeo excesivo,vómito"),
(22,"Meningitis","pérdida de coordinación, elevada fiebre"),
(23,"Periodontitis","acumulación de sarro y placa,infecciones graves o pérdida de dientes"),
(24,"Piómetra","secreciones de pus a través de la vagina"),
(25,"Cistitis","micciones frecuentes y escasas,posible presencia de sangre,hiperactividad,molestias o dolor al miccionar"),
(26,"Torsión gástrica","dilatación y torsión del estómago,intentos de vómito y náuseas,salivación abundante,abdomen dilatado"),
(27,"Dermatitis alérgica a la picadura de pulga","reacción de hipersensibilidad a la saliva de las pulgas,heridas y alopecia"),
(28,"Dermatitis atópica","hipersensibilidad por ácaros,polen,polvo,aparición de prurito,secreción nasal,ocular,estornudos u otitis,pies con manchas marrones"),
(29,"Hongos","alopecias circulares,costras y escamas de un color amarillento"),
(30,"Diabetes","sed abundante,adelgaza fácilmente,cataratas,orina con frecuencia"),
(31,"Leucemia","falta de apetito,somnolencia,anemia,aparición de tumores,debilidad"),
(32,"Panleucopenia","fiebre,hipotermia,vómitos,diarrea,debilidad,deshidratación,anorexia,bajada de leucocitos y/o glóbulos blancos"),
(33,"Inmunodeficiencia","infecciones en la boca, patologías respiratorias, infecciones intestinales, pérdida de mucho peso, enfermedades fúngicas"),
(34,"Peritonitis","fiebre,anorexia,aumento del volumen del abdomen,acumulación de líquido invadiendo órganos y sistemas del cuerpo"),
(35,"Alergias","tos, estornudos, secreción nasal, secreción ocular, picor en la nariz, picor en los ojos, falta de pelo, infecciones cutáneas"),
(36,"Insuficiencia renal","falta de apetito,bebe mucha agua,el pelaje se deteriora,vómitos,halitosis,ulceraciones en la boca"),
(37,"Cataratas","pérdida de transparencia del cristalino,inseguridad a la hora de caminar,excesiva humedad en los ojos,cambios en el color del iris"),
(38,"Dermatitis","picazón,piel irritada e inflamada, pequeños bultos, sobre todo, en cara cuello y dorso"),
(39,"Tiña","pérdida de pelo en parches circulares de la piel como en: cabeza,orejas y patas,parches de piel sin pelo inflamada y escamosa,comezón constante"),
(40,"Criptococosis","tos,estornudos,dificultad para respirar,sangrado de nariz,hinchazón de cara y nariz,granulomas subcutáneos,falta de coordinación,ceguera,convulsiones"),
(41,"Histoplasmosis","falta de coordinación para caminar,fiebre,decaimiento,debilidad,malestar respiratorio,tos seca,afección de los ojos"),
(42,"Esporotricosis","heridas y nódulos en la piel sin pelo en cara, patas y cola,inflamación de ganglios linfáticos,malestar general,fiebre"),
(43,"Gripe","estornudos, moquillo y lagrimeo"),
(44,"Rinotraqueitis","fiebre, estornudos, mucosidad nasal, conjuntivitis, lagrimeo, úlceras en la córnea"),
(45,"Calicivirosis","estornudos, fiebre, mucha salivación, úlceras, ampollas en la boca y la lengua"),
(46,"Neumonitis","fiebre, tos, mucosidad nasal y falta de apetito");


/*Carga de servicios Veterinarias*/
INSERT INTO Servicios(id_veterinaria,descripcion) VALUES 
(1,"Consulta médica"),
(2,"Consulta médica"),
(3,"Consulta médica"),
(1,"Estética"),
(2,"Estética"),
(1,"Laboratorio"),
(1,"Atención a domicilio"),
(3,"Atención a domicilio");