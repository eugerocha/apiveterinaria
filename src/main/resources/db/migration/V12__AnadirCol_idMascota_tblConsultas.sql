/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  euge
 * Created: 26/02/2020
 */

/*Agregar campo id mascota a tabla consultas*/
ALTER TABLE Consultas ADD id_mascota INT NOT NULL AFTER estado;

/*Definir id mascota com oclave foránea en tabla Consultas*/

ALTER TABLE Consultas ADD CONSTRAINT fk_mascotas_consultas FOREIGN KEY(id_mascota) REFERENCES Mascotas(id) ON UPDATE CASCADE;