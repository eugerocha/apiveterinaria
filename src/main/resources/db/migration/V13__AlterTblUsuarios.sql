/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  euge
 * Created: 26/02/2020
 */
/*Modificar campos tabla Usuarios*/
ALTER TABLE Usuarios ADD UUID VARCHAR(100) NOT NULL AFTER id;
ALTER TABLE Usuarios MODIFY password VARCHAR(256);
ALTER TABLE Usuarios MODIFY fecha_alta DATETIME;
ALTER TABLE Usuarios MODIFY ultimo_login DATETIME;



/*Insertar usuarios*/

INSERT INTO Usuarios() VALUES
(1,"u1","euge","123456","2020-02-27 12:00",1,"2020-02-27 12:00"),
(2,"u2","jaz","456789","2020-02-27 12:00",1,"2020-02-27 12:00"),
(3,"u3","marce","111222","2020-02-27 12:00",1,"2020-02-27 12:00");


