/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  euge
 * Created: 26/02/2020
 */

/*Crear tabla Roles-Usuarios*/

CREATE TABLE UsuariosVeterinarias(
    id_veterinaria INT NOT NULL ,
    id_usuario BIGINT UNSIGNED NOT NULL,
    id_rol INT,
    PRIMARY KEY(id_veterinaria,id_usuario,id_rol)
);


/*Establecer claves foráneas*/
ALTER TABLE UsuariosVeterinarias ADD CONSTRAINT fk_veterinarias_veterinariasUsuarios FOREIGN KEY(id_veterinaria) REFERENCES Veterinarias(id) ON UPDATE CASCADE;
ALTER TABLE UsuariosVeterinarias ADD CONSTRAINT fk_usuarios_veterinariasUsuarios FOREIGN KEY(id_usuario) REFERENCES Usuarios(id) ON UPDATE CASCADE;
ALTER TABLE UsuariosVeterinarias ADD CONSTRAINT fk_roles_veterinariasUsuarios FOREIGN KEY(id_rol) REFERENCES Roles(id) ON UPDATE CASCADE;


/*Inserción prueba tabla UsuariosVeterinarias*/
INSERT INTO UsuariosVeterinarias() VALUES
(1,1,1),
(2,1,3),
(1,2,2);