/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  euge
 * Created: 26/02/2020
 */

/*Modificar campos de tabla CalendarioVacunas*/

ALTER TABLE CalendarioVacunas ADD fecha_carga DATE NOT NULL AFTER id_vacuna;

ALTER TABLE CalendarioVacunas CHANGE fecha fecha_aplicacion DATETIME;

ALTER TABLE CalendarioVacunas ADD id_profesional BIGINT UNSIGNED NOT NULL AFTER id_mascota;


/*Establecer clave foránea id_profesional*/

ALTER TABLE CalendarioVacunas ADD CONSTRAINT fk_veterinarios_calendario FOREIGN KEY(id_profesional) REFERENCES Profesionales(id_profesional) ON UPDATE CASCADE;





 