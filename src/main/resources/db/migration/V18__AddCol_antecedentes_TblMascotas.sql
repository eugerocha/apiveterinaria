/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  euge
 * Created: 27/02/2020
 */

/*Agregar campos antecedentes en tabla Mascotas*/

ALTER TABLE Mascotas ADD ant_quirurgicos  VARCHAR(300) AFTER peso;

ALTER TABLE Mascotas ADD ant_patologicos  VARCHAR(300) AFTER ant_quirurgicos;

ALTER TABLE Mascotas ADD ant_traumatologicos  VARCHAR(300) AFTER ant_patologicos;
