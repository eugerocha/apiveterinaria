CREATE TABLE  Afecciones  (
   id  INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
   nombre  VARCHAR(50),
   sintomas  VARCHAR(500)
);


CREATE TABLE  Diagnosticos  (
   id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
   diagnostico_primario  VARCHAR(300),
   id_consulta INT NOT NULL,
   id_afeccion INT NOT NULL
);


CREATE TABLE  CalendarioVacunas  (
   id  INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
   fecha  DATE,
   dosis  INT(11),
   observaciones  VARCHAR(300),
   id_mascota INT NOT NULL,
   id_vacuna  INT NOT NULL
) ;


CREATE TABLE  Consultas  (
   id  INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
   fecha  DATE,
   hora  TIME,
   motivo  VARCHAR(300),
   estado INT(11) NOT NULL,
   id_historia INT,
   id_profesional BIGINT UNSIGNED NOT NULL
);


CREATE TABLE  HistoriasClinicas  (
   id  INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
   antecedentes_quirurgicos  VARCHAR(300),
   antecedentes_patologicos  VARCHAR(300),
   antecedentes_traumatologicos  VARCHAR(300),
   id_mascota INT NOT NULL
);


CREATE TABLE  Mascotas  (
   id  INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
   nombre  VARCHAR(30),
   sexo  INT NOT NULL,
   fechanacimiento DATE,
   peso  FLOAT,
   imagen VARCHAR(150),
   id_cliente BIGINT UNSIGNED NOT NULL,
   id_tipo INT NOT NULL,
   id_raza INT NOT NULL
);

CREATE TABLE  Prescripciones  (
   id  INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
   producto  VARCHAR(20),
   cantidad  VARCHAR(20),
   indicaciones  VARCHAR(300),
   id_consulta INT NOT NULL
);


CREATE TABLE  Razas  (
   id  INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
   nombre_raza  VARCHAR(20),
   tipo INT NOT NULL
);

CREATE TABLE TipoMascota (
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre_tipo VARCHAR(20)
);

CREATE TABLE  Turnos  (
   id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
   fecha  DATETIME,
   id_profesional BIGINT UNSIGNED NOT NULL,
   id_mascota INT NOT NULL
);


CREATE TABLE  Vacunas  (
   id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
   nombre  VARCHAR(30),
   edad_aplicacion  VARCHAR(30),
   detalle VARCHAR(150),
   tipo_mascota INT NOT NULL
);


CREATE TABLE  Visitas  (
   id  INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
   fecha_visita  DATE   ,
   tipo_visita   VARCHAR(30),
   id_mascota INT NOT NULL 
);


CREATE TABLE MascotasVisitas (
    id_mascota INT NOT NULL,
    id_visita INT NOT NULL

);

CREATE TABLE Personas (
    id BIGINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    apellido VARCHAR(100) NOT NULL,
    nombre VARCHAR(100) NOT NULL,
    dni VARCHAR(20) NOT NULL,
    tipodni INT NOT NULL,
    sexo INT NOT NULL,
    telefono VARCHAR(30),
    email VARCHAR(80),
    fnacimiento DATE NOT NULL,
    falta DATETIME NOT NULL,
    imagen VARCHAR(150)
);


CREATE TABLE Clientes (
    id BIGINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    codigo_cliente INT NOT NULL, 
    fbaja DATETIME,
    id_veterinaria INT,    
    estado INT NOT NULL
);


CREATE TABLE Profesionales (
    id_profesional BIGINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    legajo VARCHAR(15),
    estado INT NOT NULL
);


CREATE TABLE HorarioAtencion(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    dia INT NOT NULL,
    hora TIME NOT NULL,
    id_profesional BIGINT UNSIGNED NOT NULL
);


CREATE TABLE Usuarios (
    id BIGINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(15),
    password VARCHAR(15),
    fecha_alta DATE, 
    estado INT NOT NULL,
    ultimo_login DATETIME NOT NULL
);


CREATE TABLE Domicilios(
    id_domicilio INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    calle VARCHAR(15),
    numero VARCHAR(5),
    barrio INT,
    piso INT,
    depto VARCHAR(5),
    id_persona BIGINT UNSIGNED NOT NULL,
    estado INT NOT NULL
);


CREATE TABLE Veterinarias(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    domicilio VARCHAR(15),
    telefono VARCHAR(15),
    email VARCHAR(30),
    encargado VARCHAR(50),
    horario VARCHAR(15)
);

CREATE TABLE Servicios(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    id_veterinaria INT NOT NULL,
    descripcion VARCHAR(50)
);