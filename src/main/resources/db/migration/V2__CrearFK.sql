ALTER TABLE Diagnosticos ADD CONSTRAINT fk_consultas_diagnosticos FOREIGN KEY(id_consulta) REFERENCES Consultas(id) ON UPDATE CASCADE;

ALTER TABLE Diagnosticos ADD CONSTRAINT fk_afecciones_diagnosticos FOREIGN KEY(id_afeccion) REFERENCES Afecciones(id) ON UPDATE CASCADE;

ALTER TABLE CalendarioVacunas ADD CONSTRAINT fk_mascotas_calendarios FOREIGN KEY(id_mascota) REFERENCES Mascotas(id) ON UPDATE CASCADE;

ALTER TABLE CalendarioVacunas ADD CONSTRAINT fk_vacunas_calendarios FOREIGN KEY(id_vacuna) REFERENCES Vacunas(id) ON UPDATE CASCADE;

ALTER TABLE Consultas ADD CONSTRAINT fk_veterinarios_consultas FOREIGN KEY(id_profesional) REFERENCES Profesionales(id_profesional) ON UPDATE CASCADE;

ALTER TABLE Consultas ADD CONSTRAINT fk_historias_consultas FOREIGN KEY(id_historia) REFERENCES HistoriasClinicas(id) ON UPDATE CASCADE;

ALTER TABLE HistoriasClinicas ADD CONSTRAINT fk_mascotas_historias FOREIGN KEY(id_mascota) REFERENCES Mascotas(id) ON UPDATE CASCADE;

ALTER TABLE Mascotas ADD CONSTRAINT fk_clientes_mascotas FOREIGN KEY(id_cliente) REFERENCES Clientes(id) ON UPDATE CASCADE;

ALTER TABLE Mascotas ADD CONSTRAINT fk_razas_mascotas FOREIGN KEY(id_raza) REFERENCES Razas(id) ON UPDATE CASCADE;

ALTER TABLE Mascotas ADD CONSTRAINT fk_tipoMascota_mascotas FOREIGN KEY(id_tipo) REFERENCES TipoMascota(id) ON UPDATE CASCADE;

ALTER TABLE Prescripciones ADD CONSTRAINT fk_consultas_prescripciones FOREIGN KEY(id_consulta) REFERENCES Consultas(id) ON UPDATE CASCADE;

ALTER TABLE Razas ADD CONSTRAINT fk_tipo_razas FOREIGN KEY(tipo) REFERENCES TipoMascota(id) ON UPDATE CASCADE;

ALTER TABLE Turnos ADD CONSTRAINT fk_mascotas_turnos FOREIGN KEY(id_mascota) REFERENCES Mascotas(id) ON UPDATE CASCADE;

ALTER TABLE Turnos ADD CONSTRAINT fk_profesionales_turnos FOREIGN KEY(id_profesional) REFERENCES Profesionales(id_profesional) ON UPDATE CASCADE;

ALTER TABLE Visitas ADD CONSTRAINT fk_mascotas_visitas FOREIGN KEY(id_mascota) REFERENCES Mascotas(id) ON UPDATE CASCADE;

ALTER TABLE MascotasVisitas ADD CONSTRAINT fk_mascotas_mascotasVisitas FOREIGN KEY(id_mascota) REFERENCES Mascotas(id) ON UPDATE CASCADE;

ALTER TABLE MascotasVisitas ADD CONSTRAINT fk_visitas_mascotasVisitas FOREIGN KEY(id_visita) REFERENCES Visitas(id) ON UPDATE CASCADE;

ALTER TABLE Usuarios ADD CONSTRAINT fk_personas_usuarios FOREIGN KEY(id) REFERENCES Personas(id) ON UPDATE CASCADE;

ALTER TABLE Clientes ADD CONSTRAINT fk_personas_clientes FOREIGN KEY(id) REFERENCES Personas(id) ON UPDATE CASCADE;

ALTER TABLE Profesionales ADD CONSTRAINT fk_personas_profesionales FOREIGN KEY(id_profesional) REFERENCES Personas(id) ON UPDATE CASCADE;

ALTER TABLE Domicilios ADD CONSTRAINT fk_personas_domicilios FOREIGN KEY(id_persona) REFERENCES Personas(id) ON UPDATE CASCADE;

ALTER TABLE HorarioAtencion ADD CONSTRAINT fk_profesionales_horarioAtencion FOREIGN KEY(id_profesional) REFERENCES Profesionales(id_profesional) ON UPDATE CASCADE;

ALTER TABLE Servicios ADD CONSTRAINT fk_veterinarias_servicios FOREIGN KEY(id_veterinaria) REFERENCES Veterinarias(id) ON UPDATE CASCADE;

ALTER TABLE Vacunas ADD CONSTRAINT fk_tipoMascota_vacunas FOREIGN KEY(tipo_mascota) REFERENCES TipoMascota(id) ON UPDATE CASCADE;