/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  euge
 * Created: 12/01/2020
 */

/*Agrego nueva persona*/
INSERT INTO Personas() VALUES (3,"Rocha","Jazmin","49797768",1,0,"123456","jr@gmail.com","2009-09-15","2020-01-12 3:00:00","......");

/*Lo agrego como cliente*/
INSERT INTO Clientes (id, codigo_cliente, estado) VALUES (3,2,1);

/*Agrego nueva mascota*/
INSERT INTO Mascotas (nombre, sexo, fechanacimiento, peso, id_cliente, id_tipo, id_raza) VALUES 
("dorita", 0, "2019-03-01", 2, 3, 2, 2);



/*Carga tabla vacunas*/
INSERT INTO Vacunas(nombre,edad_aplicacion,detalle,tipo_mascota) VALUES
("Quíntuple","45 días - 60 días","Parvovirus-coronavirus-moquillo-hepatitis infecciosa-tos de las perreras",1),
("Séxtuple","85 días - 110 días","Quíntuple + Leptospirosis",1),
("Séxtuple","adulto","Quíntuple + Leptospirosis",1),
("Antirrábica","120 días","obligatoria por ley 6515",1),
("Antirrábica","adulto","obligatoria por ley 6515",1),
("Bordetella bronchiseptica","150 días","Traqueobronquitis",1),
("Bordetella bronchiseptica","anual","Traqueobronquitis",1),
("Desparasitación","21 días","",1),
("Desparasitación","adulto","",1),
("Triple viral","2 meses","Calicivirus-panleucopenia-rinotraqueitis",2),
("Leucemia felina","10 semanas","",2),
("Triple viral","adulto","Calicivirus-panleucopenia-rinotraqueitis",2),
("Antirrábica","4 meses","",2),
("Antirrábica","adulto","",2),
("Desparasitación","21 días","",2),
("Desparasitación","adulto","",2);


