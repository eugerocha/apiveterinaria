/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  euge
 * Created: 11/02/2020
 */
ALTER TABLE Veterinarias DROP COLUMN horario;

ALTER TABLE Veterinarias ADD cuit VARCHAR(14) NOT NULL AFTER domicilio;

ALTER TABLE Veterinarias ADD estado INT NOT NULL AFTER encargado;

ALTER TABLE Veterinarias ADD imagen VARCHAR(150) AFTER estado;

/*Agrego nueva veterinaria*/
INSERT INTO Veterinarias(id,razonSocial,domicilio,cuit,telefono,email,encargado,estado) VALUES 
(1, "Alem", "Catamarca 299", "10-11222333-0", "156156", "alem@gmail.com", "Juancito", 1),
(2,"Abril","Moreno 756","11-45789789-1","15412512500","vet.abril@gmail.com","Jorge",1),
(3,"Batuque","Belgrano 1245","7-35218102-2","422570","batuquesgo@gmail.com","Marisa",1);

