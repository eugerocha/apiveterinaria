/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  euge
 * Created: 12/02/2020
 */

CREATE TABLE HorariosVeterinarias (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    dia INT,
    apertura TIME,
    cierre TIME,
    observaciones VARCHAR(150), 
    id_vet INT NOT NULL
);

ALTER TABLE HorariosVeterinarias ADD CONSTRAINT fk_veterinarias_horariosVeterinarias FOREIGN KEY(id_vet) REFERENCES Veterinarias(id) ON UPDATE CASCADE;

INSERT INTO HorariosVeterinarias () VALUES 
(1,6,"09:00:00","12:00:00", "feriados cerrado", 1),
(2,1,"08:00:00","13:00:00","feriados hasta las 10",1),
(3,2,"08:00:00","13:00:00","",1),
(4,1,"09:00:00","14:00:00","por la tarde cerrado",2),
(5,2,"09:00:00","14:00:00","",2);

